
import utilities.AppUtilities;
import utilities.HttpUtilities;
import utilities.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rubenpanga
 */
public class Test {

    private static final String TAG = "Test";

    public static void main(String... args) {
        try {
            new Thread(
                    new Runnable() {
                @Override
                public void run() {
                    try {
                        Logger.printLog(TAG, "Dans le thread 1");
                        Thread.sleep(2000);
                        Logger.printLog(TAG, "Dans le thread 2");
                        Runtime.getRuntime().exit(0);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            ).start();
            Logger.printLog(TAG, "en dehors");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }
}
