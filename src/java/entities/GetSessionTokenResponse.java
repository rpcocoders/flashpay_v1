/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author rubenpanga
 */
public class GetSessionTokenResponse {
    private Long idagent;
    private String token;

    public Long getIdagent() {
        return idagent;
    }

    public void setIdagent(Long idagent) {
        this.idagent = idagent;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
