/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author g.mbuyi
 */
public class PayementEloctroniqueResponseEntity {
    
    private String urlRedict;
    private String orderId;

    public PayementEloctroniqueResponseEntity(String urlRedict) {
        this.urlRedict = urlRedict;
    }

    public PayementEloctroniqueResponseEntity() {
    }

    public String getUrlRedict() {
        return urlRedict;
    }

    public void setUrlRedict(String urlRedict) {
        this.urlRedict = urlRedict;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    
    
    
}
