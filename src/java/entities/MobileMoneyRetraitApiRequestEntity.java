/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author jonathanmulumba
 */
public class MobileMoneyRetraitApiRequestEntity {
    private Long transactionId;
    private String devise;
    private String produit;
    private String date;
    private String sender;
    private Double montant;
    private Double Commission;
    private String transactionIdExt;
    private String statut;
    private Double commissionCFC;

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Double getCommission() {
        return Commission;
    }

    public void setCommission(Double Commission) {
        this.Commission = Commission;
    }

    public String getTransactionIdExt() {
        return transactionIdExt;
    }

    public void setTransactionIdExt(String transactionIdExt) {
        this.transactionIdExt = transactionIdExt;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Double getCommissionCFC() {
        return commissionCFC;
    }

    public void setCommissionCFC(Double commissionCFC) {
        this.commissionCFC = commissionCFC;
    }
    
    
}
