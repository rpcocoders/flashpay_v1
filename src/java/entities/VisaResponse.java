/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author hp
 */
public class VisaResponse {
    private Long id;
    private Long agent;
    private String urlRedict;
    private String orderId;

    public String getUrlRedict() {
        return urlRedict;
    }

    public void setUrlRedict(String urlRedict) {
        this.urlRedict = urlRedict;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAgent() {
        return agent;
    }

    public void setAgent(Long agent) {
        this.agent = agent;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    
    
}
