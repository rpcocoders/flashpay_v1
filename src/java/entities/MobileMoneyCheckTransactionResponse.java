/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author hp
 */
public class MobileMoneyCheckTransactionResponse {
    private String receiver;
    private String sender;
    private Double montant;
    private Double commission;
    private String devise;
    private String idExtTransaction;
    private Double commissionCFC;
    private Long dTransaction;
    private String status;

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getIdExtTransaction() {
        return idExtTransaction;
    }

    public void setIdExtTransaction(String idExtTransaction) {
        this.idExtTransaction = idExtTransaction;
    }

    public Double getCommissionCFC() {
        return commissionCFC;
    }

    public void setCommissionCFC(Double commissionCFC) {
        this.commissionCFC = commissionCFC;
    }

    public Long getdTransaction() {
        return dTransaction;
    }

    public void setdTransaction(Long dTransaction) {
        this.dTransaction = dTransaction;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
