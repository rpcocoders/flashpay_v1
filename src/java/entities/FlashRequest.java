/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author hp
 */
public class FlashRequest {

    private String urlSuccess;
    private String urlDecline;
    private String login;
    private String pwd;
    private String codePin;
    private String token;
    private Double montant;
    private String deviseiso;
    private String mobile;
    private Long agent;
    private String commentaire;
    private String phone;
    private String fpid;
    private String fpid_client;
    private String id;
    private String type;
    private String idExt;
    private Double frais;
    private String posid;
    private String devise;
    private String typetransaction;
    private String numref;
    private String codeSms;
    private String codeIn;
    
    public String getUrlSuccess() {
        return urlSuccess;
    }

    public void setUrlSuccess(String urlSuccess) {
        this.urlSuccess = urlSuccess;
    }

    public String getUrlDecline() {
        return urlDecline;
    }

    public void setUrlDecline(String urlDecline) {
        this.urlDecline = urlDecline;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCodePin() {
        return codePin;
    }

    public void setCodePin(String codePin) {
        this.codePin = codePin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getDeviseiso() {
        return deviseiso;
    }

    public void setDeviseiso(String deviseiso) {
        this.deviseiso = deviseiso;
    }

    

    public Double getFrais() {
        return frais;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }

 

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getAgent() {
        return agent;
    }

    public void setAgent(Long agent) {
        this.agent = agent;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getTypetransaction() {
        return typetransaction;
    }

    public void setTypetransaction(String typetransaction) {
        this.typetransaction = typetransaction;
    }

  

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFpid() {
        return fpid;
    }

    public void setFpid(String fpid) {
        this.fpid = fpid;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdExt() {
        return idExt;
    }

    public void setIdExt(String idExt) {
        this.idExt = idExt;
    }

    public String getFpid_client() {
        return fpid_client;
    }

    public void setFpid_client(String fpid_client) {
        this.fpid_client = fpid_client;
    }

    public String getPosid() {
        return posid;
    }

    public void setPosid(String posid) {
        this.posid = posid;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getNumref() {
        return numref;
    }

    public void setNumref(String numref) {
        this.numref = numref;
    }

    public String getCodeSms() {
        return codeSms;
    }

    public void setCodeSms(String codeSms) {
        this.codeSms = codeSms;
    }

    public String getCodeIn() {
        return codeIn;
    }

    public void setCodeIn(String codeIn) {
        this.codeIn = codeIn;
    }

  


    
    
   
  
    public static FlashRequest getInstanceFromJson(String json) {
        FlashRequest obj = new Gson().fromJson(json, new TypeToken<FlashRequest>() {
        }.getType());
        return obj;
    }

}
