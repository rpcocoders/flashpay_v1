/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author hp
 */
public class MobileMoneyRequest {

    private String phone;
    private String posid; 
    private String id;
    private String type;
    private Double montant;
    private String deviseiso;
    private Long agent;
    private String mobile;
    
    private String login;
    private String pwd;
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPosid() {
        return posid;
    }

    
    public void setPosid(String posid) {
        this.posid = posid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getDeviseiso() {
        return deviseiso;
    }

    public void setDeviseiso(String deviseiso) {
        this.deviseiso = deviseiso;
    }

    public Long getAgent() {
        return agent;
    }

    public void setAgent(Long agent) {
        this.agent = agent;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    
    

    public static MobileMoneyRequest getInstanceFromJson(String json) {
        return new Gson().fromJson(json, new TypeToken<MobileMoneyRequest>() {
        }.getType());
    }

    
}
