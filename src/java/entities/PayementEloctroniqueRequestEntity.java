/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author g.mbuyi
 */
public class PayementEloctroniqueRequestEntity {
    
    private String description;
    private Double montant ;
    private String devise;
    private Double frais;
    private String urlSuccess;
    private String urlDecline;
    private String numRef;

    public PayementEloctroniqueRequestEntity() {
    }

    
    public PayementEloctroniqueRequestEntity(String description, Double montant, String devise , Double frais) {
        this.description = description;
        this.montant = montant;
        this.devise = devise;
        this.frais = frais;
    }

    public PayementEloctroniqueRequestEntity(String description, Double montant, String devise, Double frais, String urlSuccess, String urlDecline) {
        this.description = description;
        this.montant = montant;
        this.devise = devise;
        this.frais = frais;
        this.urlSuccess = urlSuccess;
        this.urlDecline = urlDecline;
    }
    
    

    public Double getFrais() {
        return frais;
    }

    public void setFrais(Double frais) {
        this.frais = frais;
    }

    
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getUrlSuccess() {
        return urlSuccess;
    }

    public void setUrlSuccess(String urlSuccess) {
        this.urlSuccess = urlSuccess;
    }

    public String getUrlDecline() {
        return urlDecline;
    }

    public void setUrlDecline(String urlDecline) {
        this.urlDecline = urlDecline;
    }

    public String getNumRef() {
        return numRef;
    }

    public void setNumRef(String numRef) {
        this.numRef = numRef;
    }

    
    
    
    public static PayementEloctroniqueRequestEntity getInstanceFromJson(String json){
        PayementEloctroniqueRequestEntity obj = new Gson().fromJson(json, new TypeToken<PayementEloctroniqueRequestEntity>(){}.getType());
        return obj;
    }
    
    
}
