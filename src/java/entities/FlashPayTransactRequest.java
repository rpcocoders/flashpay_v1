/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author rubenpanga
 */
public class FlashPayTransactRequest {
    private Double montant;
    private String devise;
    private String urlSuccess;
    private String urlDecline;
    private String commentaire;
    private String fpid;
    private Long agent;
    private String idExt;
    private String typetransaction;
    private Long id;
    private String numref;
    private String fpid_client;
    private String phone;
    private String msg;

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getUrlSuccess() {
        return urlSuccess;
    }

    public void setUrlSuccess(String urlSuccess) {
        this.urlSuccess = urlSuccess;
    }

    public String getUrlDecline() {
        return urlDecline;
    }

    public void setUrlDecline(String urlDecline) {
        this.urlDecline = urlDecline;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getFpid() {
        return fpid;
    }

    public void setFpid(String fpid) {
        this.fpid = fpid;
    }

    public Long getAgent() {
        return agent;
    }

    public void setAgent(Long agent) {
        this.agent = agent;
    }

    public String getIdExt() {
        return idExt;
    }

    public void setIdExt(String idExt) {
        this.idExt = idExt;
    }

    public String getTypetransaction() {
        return typetransaction;
    }

    public void setTypetransaction(String typetransaction) {
        this.typetransaction = typetransaction;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumref() {
        return numref;
    }

    public void setNumref(String numref) {
        this.numref = numref;
    }

    public String getFpid_client() {
        return fpid_client;
    }

    public void setFpid_client(String fpid_client) {
        this.fpid_client = fpid_client;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    
}
