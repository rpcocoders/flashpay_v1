/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author hp
 */
public class MobileMoneyCheckTransactionRequestEntity {
    private String idExt;
    private Long idagent;

    public String getIdExt() {
        return idExt;
    }

    public void setIdExt(String idExt) {
        this.idExt = idExt;
    }

    public Long getIdagent() {
        return idagent;
    }

    public void setIdagent(Long idagent) {
        this.idagent = idagent;
    }
    
    
    
     public static MobileMoneyCheckTransactionRequestEntity getInstanceFromJson(String json) {
        return new Gson().fromJson(json, new TypeToken<MobileMoneyCheckTransactionRequestEntity>() {
        }.getType());
    }
}
