/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author hp
 */
public class AuthentificationInfoFlash {
    private String login;
    private String pwd;
    private String codePin;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCodePin() {
        return codePin;
    }

    public void setCodePin(String codePin) {
        this.codePin = codePin;
    }

     public static AuthentificationInfoFlash getInstanceFromJson(String json) {
        
        return new Gson().fromJson(json, new TypeToken<AuthentificationInfoFlash>() {
        }.getType());
    }
}
