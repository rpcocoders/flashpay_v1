/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author hp
 */
public class FlashPayTransactResponse {
    private Long id;
    private String codeSms;
    private String numref;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeSms() {
        return codeSms;
    }

    public void setCodeSms(String codeSms) {
        this.codeSms = codeSms;
    }

    public String getNumref() {
        return numref;
    }

    public void setNumref(String numref) {
        this.numref = numref;
    }
    
    
    
}
