/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import entities.AuthentificationInfoFlash;
import entities.AuthentificationInformations;
import entities.GetSessionTokenResponse;
import entities.Marchand;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.json.JSONException;
import utilities.AppConst;
import utilities.AppUtilities;
import utilities.ErrorResponse;
import utilities.HttpDataResponse;
import utilities.HttpUrls;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
public class AuthentificationController {

    private static final String TAG = AuthentificationController.class.getSimpleName();

    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public AuthentificationController() {
        this.error = new ErrorResponse();
    }

    public GetSessionTokenResponse getSession(Marchand marchand) {
        GetSessionTokenResponse session = null;

        try {
            if (marchand == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT);
            }
            
            AppUtilities.controlValue(marchand.getCodePin(), "Veuillez renseigner le pin marchand");
            AppUtilities.controlValue(marchand.getDeviseiso(), "Veuillez renseigner la devise de la transaction");
            AppUtilities.controlValue(marchand.getFpid(), "Veuillez rensienger le fpid ");
            AppUtilities.controlValue(marchand.getTransactionId(), "Veuillez renseigner l'identifiant de la transaction");
            AppUtilities.controlValue(marchand.getLogin(), "Veuillez renseigner le login marchand");
            AppUtilities.controlValue(marchand.getMontant(), "Veuillez renseigner le montant de la transaction");
            AppUtilities.controlValue(marchand.getNomMarchand(), "Nom marchand");
            AppUtilities.controlValue(marchand.getPwd(), "Veuillez renseigner le mot de passe marchand");
            AppUtilities.controlValue(marchand.getToken(), "Veuillez rensiegner le token");
            AppUtilities.controlValue(marchand.getUrlDecline(), "Veuillez renseigner le lien d'échec");
            AppUtilities.controlValue(marchand.getUrlSuccess(), "Veuillez renseigner le lien du succès");
            
            session = this.callCheckConnexionFlash(marchand);
            
            if(session == null)
                throw new ValueDataException("Échec d'authentification du marchand");
            
            if(session.getToken().equals(marchand.getToken()))
                throw new ValueDataException("Token fournis non-reconnu");
            
            marchand.setAgent(session.getIdagent());
            
            this.error.setCode("OK");

        } catch (ValueDataException e) {
            e.printStackTrace();
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setCode("KO");
            this.error.setDescription(AppConst.UNKNOWN_ERROR);
        }

        return session;
    }

    private GetSessionTokenResponse callCheckConnexionFlash(Marchand marchand) throws KeyStoreException,
            NoSuchAlgorithmException, KeyManagementException, UnsupportedEncodingException, IOException, JSONException, URISyntaxException {
        HttpDataResponse<GetSessionTokenResponse> httpDataResponse = null;

        //String url = "http://192.168.202.249:3000/api/services/get/token";
        String url = HttpUrls.API_GET_SESSION_TOKEN;

        AuthentificationInfoFlash auth = new AuthentificationInfoFlash();
        auth.setLogin(marchand.getLogin());
        auth.setPwd(marchand.getPwd());
        auth.setCodePin(marchand.getCodePin());

        HttpClient httpClient = HttpUtilities.createHttpClient_AcceptsUntrustedCerts(url);
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Content-type", "application/json");
        httpPost.addHeader("Accept", "application/json");
        httpPost.addHeader("Accept-Encoding", "UTF-8");
        httpPost.addHeader("Connection", "close");
        String json = new Gson().toJson(auth);
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        HttpResponse response = httpClient.execute(httpPost);
        int status = response.getStatusLine().getStatusCode();
        String msg = response.getStatusLine().getReasonPhrase();
        String res = HttpUtilities.readInputStream(response.getEntity().getContent());

        Logger.printLog(TAG, "Response HttpClient: " + res);

        if (status == 200) {
            httpDataResponse = new Gson().fromJson(res,
                    new TypeToken<HttpDataResponse<GetSessionTokenResponse>>() {
                    }.getType());
        }
        return httpDataResponse.getResponse();
    }

    
}
