/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import cfcdatabase.Model.Agent;
import cfcdatabase.Model.Flashpaytransaction;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import daomanagers.ControleurSagid;
import daomanagers.ControleurSms;
import daomanagers.ControlleurAgent;
import daomanagers.DaoManagerCommissionV2;
import daomanagers.TransactionFlashPayManager;
import entities.CommissionEntity;
import entities.FlashPayTransactResponse;
import entities.Marchand;
import entities.MobileMoneyCheckTransactionRequestEntity;
import entities.MobileMoneyCheckTransactionResponse;
import entities.MobileMoneyRequest;
import entities.MobileMoneyRetraitApiRequestEntity;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import entities.FlashRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;
import utilities.AppConst;
import utilities.AppUtilities;
import utilities.ErrorResponse;
import utilities.HttpDataResponse;
import utilities.HttpUrls;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
public class TransactionController {

    private ErrorResponse error;

    public static final String TYPE_FLASH = "FLASH";
    public static final String COMMENT_FLASH = "PAIEMENT FLASH";
    public static final String TYPE_MOBILE_MONEY = "MOBILE MONEY";
    public static final String COMMENT_MOBILE_MONEY = "PAIEMENT MOBILE MONEY";
    public static final String TYPE_VISA_CARD = "VISA";
    public static final String COMMENT_VISA_CARD = "PAIEMENT VISA";
    public static final String TYPE_FLASHPAY = "FLASH PAY";
    public static final String COMMENT_FLASHPAY = "PAIEMENT CASH FLASH PAY";

    private static final String STATUT_SUCCESS = "SUCCESS";
    private static final String STATUT_PENDING = "PENDING";

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public TransactionController() {
        this.error = new ErrorResponse();
    }

    public FlashPayTransactResponse initiatePaiement(Marchand marchand, String fpi) {
        FlashPayTransactResponse codeValidation = null;

        try {

            AppUtilities.controlValue(fpi, "Veuillez renseigner le fpi");

            FlashRequest request = new FlashRequest();

            codeValidation = null;

            request.setAgent(marchand.getAgent());
            request.setCommentaire(marchand.getCommentaire());
            request.setDevise(marchand.getDeviseiso());
            request.setDeviseiso(marchand.getDeviseiso());
            request.setFpid(marchand.getFpid());
            request.setFpid_client(fpi);
            request.setFrais(null);
            request.setLogin(marchand.getLogin());
            request.setMobile(null);
            request.setMontant(marchand.getMontant());
            request.setNumref(null);
            request.setPwd(marchand.getPwd());
            request.setToken(marchand.getToken());
            request.setType(TYPE_FLASH);
            request.setTypetransaction(TYPE_FLASH);
            request.setUrlDecline(marchand.getUrlDecline());
            request.setUrlSuccess(marchand.getUrlSuccess());
            request.setCodeSms(AppUtilities.randomAlphaNumeric(6));

            FlashPayTransactResponse response = this.doPaiement(request);

            if (response == null) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            codeValidation = response;
            this.error.setCode("OK");

        } catch (ValueDataException e) {
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setCode("KO");
            this.error.setDescription(AppConst.UNKNOWN_ERROR);
        }

        return codeValidation;
    }

    private FlashPayTransactResponse doPaiement(FlashRequest flashRequest) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException, UnsupportedEncodingException, IOException, JSONException, URISyntaxException {
        HttpDataResponse<FlashPayTransactResponse> httpDataResponse;
        FlashPayTransactResponse flashResponse = new FlashPayTransactResponse();

        String url = HttpUrls.API_INITIATE_TRANSACTION;

        HttpClient httpClient = HttpUtilities.createHttpClient_AcceptsUntrustedCerts(url);
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Content-type", "application/json");
        httpPost.addHeader("Accept", "application/json");
        httpPost.addHeader("Accept-Encoding", "UTF-8");
        httpPost.addHeader("Connection", "close");
        String json = new Gson().toJson(flashRequest);
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);

        HttpResponse response = httpClient.execute(httpPost);
        int status = response.getStatusLine().getStatusCode();
        String msg = response.getStatusLine().getReasonPhrase();
        String res = HttpUtilities.readInputStream(response.getEntity().getContent());
        System.out.println("ID MESS " + res);
        if (status == 200) {
            httpDataResponse = new Gson().fromJson(res,
                    new TypeToken<HttpDataResponse<FlashPayTransactResponse>>() {
                    }.getType());

            flashResponse = httpDataResponse.getResponse();
            httpDataResponse.setResponse(flashResponse);
            httpDataResponse.setError(httpDataResponse.getError());
            System.out.println("ID TRANSACTION " + flashResponse.getId());
        } else {

            httpDataResponse = new HttpDataResponse<>();
            httpDataResponse.setError(httpDataResponse.getError());
            httpDataResponse.setResponse(null);
        }
        return httpDataResponse.getResponse();
    }

    public boolean confirmPaiement(String numRef, Long agent) {
        boolean confirmed = true;

        try {

            AppUtilities.controlValue(numRef, "Veuillez saisir le numero de reference");
            AppUtilities.controlValue(agent, "Veuillez renseigner l'agent");

        } catch (ValueDataException e) {
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            this.error.setCode("KO");
            this.error.setDescription(AppConst.UNKNOWN_ERROR);
        }

        return confirmed;
    }

    public static FlashPayTransactResponse confirm(FlashPayTransactResponse flashRep) throws KeyStoreException,
            NoSuchAlgorithmException, KeyManagementException, UnsupportedEncodingException, IOException, JSONException, URISyntaxException {
        HttpDataResponse<FlashPayTransactResponse> httpDataResponse;
        FlashPayTransactResponse flashResponse = new FlashPayTransactResponse();

        String url = HttpUrls.API_CONFIRM_TRANSACTION;

        HttpClient httpClient = HttpUtilities.createHttpClient_AcceptsUntrustedCerts(url);
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Content-type", "application/json");
        httpPost.addHeader("Accept", "application/json");
        httpPost.addHeader("Accept-Encoding", "UTF-8");
        httpPost.addHeader("Connection", "close");
        String json = new Gson().toJson(flashRep);
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);

        HttpResponse response = httpClient.execute(httpPost);
        int status = response.getStatusLine().getStatusCode();
        String msg = response.getStatusLine().getReasonPhrase();
        String res = HttpUtilities.readInputStream(response.getEntity().getContent());

        System.out.println("ID MESS CONFIRMATION" + res);
        if (status == 200) {
            httpDataResponse = new Gson().fromJson(res,
                    new TypeToken<HttpDataResponse<FlashPayTransactResponse>>() {
                    }.getType());

            flashResponse = httpDataResponse.getResponse();
            httpDataResponse.setResponse(flashResponse);
            httpDataResponse.setError(new ErrorResponse(String.valueOf(status), msg));
            System.out.println("ID TRANSACTION CONFIRM" + flashResponse.getId());
        } else {

            httpDataResponse = new HttpDataResponse<>();
            httpDataResponse.setError(new ErrorResponse(String.valueOf(status), msg));
            httpDataResponse.setResponse(null);
        }
        return httpDataResponse.getResponse();
    }

    public JSONObject initiatePaiement(Flashpaytransaction transaction, String type, boolean confirmSms) {
        JSONObject json = null;

        try {
            if (transaction == null) {
                throw new flashstock.utility.ValueDataException("Paramètres incorrects.");
            }

            AppUtilities.controlValue(transaction.getAgent(), "Veuillez renseigner le marchand");
            AppUtilities.controlValue(transaction.getFlashid(), "Veuillez renseigner le Flash ID du marchand");
            AppUtilities.controlValue(transaction.getDevise(), "Veuillez renseigner la devise de la transaction");

            transaction.setTypetransaction(type);

            switch (type) {
                case TYPE_FLASH:
                    AppUtilities.controlValue(transaction.getFlashidclient(), "Veuillez renseigner le Flash ID du client");
                    ControleurSagid ctrlSagid = new ControleurSagid();
                    Agent client = ctrlSagid.getAgent(transaction.getFlashidclient());
                    if (client == null) {
                        throw new ValueDataException("Ce Flash ID n'existe pas");
                    }
                    transaction.setPhoneclient(client.getContact().getNumphone());
                    break;
                case TYPE_MOBILE_MONEY:
                    AppUtilities.controlValue(transaction.getPhoneclient(), "Veuiller renseigner le numero de telephone");
                    break;
                case TYPE_VISA_CARD:
                    break;
            }

            AppUtilities.controlValue(transaction.getMontant(), "Veuillez renseigner la montant de la transaction");
            AppUtilities.controlValue(transaction.getTransactionid(), "Le marchand doit renseigner l'identifiant de la transaction");
            AppUtilities.controlValue(transaction.getTypetransaction(), "FlashPay - Veuillez renseigner le type de la paiement");
            AppUtilities.controlValue(transaction.getUrldecline(), "Le marchand doit renseigner le lien de redirection en cas d'échec de la transaction");
            AppUtilities.controlValue(transaction.getUrlsuccess(), "Le marchand doit renseigner le lien de redirection en cas du succès de la transaction");
            //AppUtilities.controlValue(transaction., message);

            //controle agent
            ControlleurAgent ctrlAgent = new ControlleurAgent();
            Agent agent = new ControlleurAgent().getAgent(transaction.getAgent());
            if (agent == null) {
                throw new ValueDataException("Cet agent n'existe pas");
            }

            //traitement commission
            DaoManagerCommissionV2 daoManagerCommissionV2 = new DaoManagerCommissionV2();

            CommissionEntity commission = daoManagerCommissionV2.getCommissionEntity(agent.getAgence().getCodeagence(), "FLASHPAYTRANSACTION", "SERVICE");

            if (commission == null) {
                commission = new CommissionEntity();
                commission.setCommission(0d);
                commission.setCommissionFcn(0d);
            }

            transaction.setPourcentagecom(commission.getCommission());
            transaction.setPourcentagecomfcn(commission.getCommissionFcn());
            transaction.setNumref("-");
            transaction.setStatut(STATUT_PENDING);

            TransactionFlashPayManager manager = new TransactionFlashPayManager();

            Long id = manager.save(transaction);

            AppUtilities.controlValue(id, AppConst.OPERATION_FAILED);

            transaction.setId(id);

            json = new JSONObject();
            json.put("id", id);

            if (confirmSms) {
                String codeValidation = AppUtilities.randomAlphaNumeric(6);
                ControleurSms ctrlSms = new ControleurSms();

                if (ctrlSms.sendSms(transaction.getPhoneclient(), "CODE DE VALIDATION FLASHPAY: " + codeValidation)) {
                    json.put("codeValidation", codeValidation);
                }
            }

            this.error.setCode("OK");

        } catch (ValueDataException e) {
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            Logger.printLog("TAG HERE", "Ici");
            e.printStackTrace();
            this.error.setCode("KO");
            this.error.setDescription("Une erreur s'est produite");
        }

        return json;
    }

    public boolean initPaiementMobileMoney(Marchand marchand, String telephone, String type) {
        boolean done = false;

        try {
            Flashpaytransaction transaction = buildTransaction(marchand, TYPE_MOBILE_MONEY);
            transaction.setPhoneclient(telephone);
            JSONObject json = this.initiatePaiement(transaction, TYPE_MOBILE_MONEY, false);

            if (json == null) {
                throw new ValueDataException(this.error.getDescription());
            }
            if (!json.has("id")) {
                throw new ValueDataException(this.error.getDescription());
            }

            Long id = json.getLong("id");

            MobileMoneyRequest mobileRequest = new MobileMoneyRequest();
            mobileRequest.setAgent(transaction.getAgent());
            mobileRequest.setDeviseiso(transaction.getDevise());
            mobileRequest.setLogin(marchand.getLogin());
            mobileRequest.setMontant(transaction.getMontant());
            mobileRequest.setPhone(telephone);
            mobileRequest.setPosid(marchand.getPwd());
            mobileRequest.setType(type);

            MobileMoneyRetraitApiRequestEntity apiMobileMoneyResponse = this.doPaiementMobileMoney(mobileRequest);

            if (apiMobileMoneyResponse == null) {
                throw new ValueDataException("Mobile Money non-disponible");
            }
            
            MobileMoneyCheckTransactionRequestEntity checkStatusEntity = new MobileMoneyCheckTransactionRequestEntity();
            
            checkStatusEntity.setIdagent(mobileRequest.getAgent());
            checkStatusEntity.setIdExt(checkStatusEntity.getIdExt());
            
            new CheckStatusMobileMoneyTask(checkStatusEntity, id, transaction).start();
            
            this.error.setCode("OK");
            
        } catch (ValueDataException e) {
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            this.error.setCode("KO");
            this.error.setDescription(AppConst.UNKNOWN_ERROR);
        }

        return done;
    }

    public boolean confirmPaiementFlashpay(Long id) {
        boolean confirmed = false;

        try {

            AppUtilities.controlValue(id, "FLASH PAY - Veuillez renseigner la transaction");

            TransactionFlashPayManager manager = new TransactionFlashPayManager();

            Flashpaytransaction transaction = manager.get(id);

            if (transaction == null) {
                throw new ValueDataException("Cette transaction n'existe pas dans le système");
            }

            transaction.setStatut(STATUT_SUCCESS);

            confirmed = manager.update(transaction);

            if (!confirmed) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setCode("OK");

        } catch (ValueDataException e) {
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setCode("KO");
            this.error.setDescription(AppConst.UNKNOWN_ERROR);
        }

        return confirmed;
    }

    public static Flashpaytransaction buildTransaction(Marchand marchand, String type) {
        Flashpaytransaction transaction = new Flashpaytransaction();

        transaction.setAgent(marchand.getAgent());
        transaction.setTypetransaction(type);
        switch (type) {
            case TYPE_FLASH:
                transaction.setCommentaire(COMMENT_FLASH);
                break;
            case TYPE_FLASHPAY:
                transaction.setCommentaire(COMMENT_FLASHPAY);
                break;
            case TYPE_MOBILE_MONEY:
                transaction.setCommentaire(COMMENT_MOBILE_MONEY);
            case TYPE_VISA_CARD:
                transaction.setCommentaire(COMMENT_VISA_CARD);
                break;
        }

        transaction.setDate(new Date());
        transaction.setDevise(marchand.getDeviseiso());
        transaction.setFlashid(marchand.getFpid());
        //transaction.setFlashidclient(type);
        transaction.setMontant(marchand.getMontant());
        //transaction.setNumref(marchand.getNomMarchand());
        //transaction.setPhoneclient(marchand.setPin(type));
        transaction.setTransactionid(marchand.getTransactionId());
        transaction.setUrldecline(marchand.getUrlDecline());
        transaction.setUrlsuccess(marchand.getUrlSuccess());

        return transaction;
    }

    public MobileMoneyRetraitApiRequestEntity doPaiementMobileMoney(MobileMoneyRequest request) {
        MobileMoneyRetraitApiRequestEntity response = null;

        try {
            AppUtilities.controlValue(request.getAgent(), "Veuillez renseigner l'agent");
            AppUtilities.controlValue(request.getDeviseiso(), "Veuillez renseigner la devise");
            AppUtilities.controlValue(request.getLogin(), "Veuillez renseigner le login");
            AppUtilities.controlValue(request.getMobile(), "Veuillez renseigner le téléphone");
            AppUtilities.controlValue(request.getMontant(), "Veuillez resneigner le montant");
            AppUtilities.controlValue(request.getPhone(), "Veuillezrenseigner le mobile");
            AppUtilities.controlValue(request.getPosid(), "Veuilez renseigner le posid");
            AppUtilities.controlValue(request.getPwd(), "Veuillez renseigner le password");
            AppUtilities.controlValue(request.getType(), "Veuillez renseigner le type");
            AppUtilities.controlValue(request.getId(), "Veuillez rensienger l'id");

            String url = HttpUrls.API_RETRAIT_MOBILE_MONEY;

            HttpClient httpClient = HttpUtilities.createHttpClient_AcceptsUntrustedCerts(url);
            HttpPost httpPost = new HttpPost(url);

            httpPost.addHeader("Accept-Encoding", "UTF-8");
            httpPost.addHeader("Content-type", "application/json");
            httpPost.addHeader("Accept", "application/json");

            httpPost.addHeader("Connection", "close");
            String json = new Gson().toJson(request);
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);

            HttpResponse httpClientResponse = httpClient.execute(httpPost);
            int status = httpClientResponse.getStatusLine().getStatusCode();
            String msg = httpClientResponse.getStatusLine().getReasonPhrase();
            String res = HttpUtilities.readInputStream(httpClientResponse.getEntity().getContent());

            HttpDataResponse<MobileMoneyRetraitApiRequestEntity> httpResponse = null;

            if (status == 200) {
                httpResponse = new Gson().fromJson(res, new TypeToken<MobileMoneyRetraitApiRequestEntity>() {
                }.getType());
            }

            if (httpResponse == null) {
                throw new ValueDataException("Impossible de joindre l'API Mobile money");
            }

            if (!httpResponse.getError().getCode().equals("200")) {
                throw new ValueDataException(httpResponse.getError().getDescription());
            }

            response = httpResponse.getResponse();
            this.error.setCode("OK");

        } catch (ValueDataException e) {
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setCode("KO");
            this.error.setDescription(AppConst.UNKNOWN_ERROR);
        }

        return response;
    }

    public MobileMoneyCheckTransactionResponse checkStatusMobileMoney(MobileMoneyCheckTransactionRequestEntity requestEntity) {
        MobileMoneyCheckTransactionResponse responseEntity = null;

        try {
            if (responseEntity == null) {
                throw new ValueDataException(AppConst.PARAMS_INCORRECT);
            }

            String url = HttpUrls.API_CHECK_STATUS_MOBILE_MONEY;

            HttpClient httpClient = HttpUtilities.createHttpClient_AcceptsUntrustedCerts(url);
            HttpPost httpPost = new HttpPost(url);

            httpPost.addHeader("Accept-Encoding", "UTF-8");
            httpPost.addHeader("Content-type", "application/json");
            httpPost.addHeader("Accept", "application/json");

            httpPost.addHeader("Connection", "close");
            String json = new Gson().toJson(requestEntity);
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);

            HttpResponse response = httpClient.execute(httpPost);
            int status = response.getStatusLine().getStatusCode();
            String msg = response.getStatusLine().getReasonPhrase();
            String res = HttpUtilities.readInputStream(response.getEntity().getContent());

            HttpDataResponse<MobileMoneyCheckTransactionResponse> httpResponse = null;
            
            if (status == 200) {
                httpResponse = new Gson().fromJson(res,
                        new TypeToken<MobileMoneyCheckTransactionResponse>() {
                        }.getType());
            }
            
            if(httpResponse == null)
                throw new ValueDataException("La request vers l'api mobile money n'a pas abouti");
            
            if(!httpResponse.getError().getCode().equals("200"))
                throw new ValueDataException(httpResponse.getError().getDescription());
            
            responseEntity = httpResponse.getResponse();
            this.error.setCode("OK");

        } catch (ValueDataException e) {
            this.error.setCode("KO");
            this.error.setDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setCode("KO");
            this.error.setDescription(AppConst.UNKNOWN_ERROR);
        }

        return responseEntity;
    }
    
    class CheckStatusMobileMoneyTask extends Thread{
        
        private MobileMoneyCheckTransactionRequestEntity request;
        private Long id;
        private Flashpaytransaction transaction;
        
        public CheckStatusMobileMoneyTask(MobileMoneyCheckTransactionRequestEntity pEntity, Long id, Flashpaytransaction transaction){
            this.request = pEntity;
            this.id = id;
            this.transaction = transaction;
        }
        
        @Override
        public void run(){
            try{
                Thread.sleep(60000);
            }catch(Exception e){}
            
            MobileMoneyCheckTransactionResponse response = checkStatusMobileMoney(request);
            
            if(response != null){
                if(confirmPaiementFlashpay(this.id))
                    new ControleurSms().sendSms(this.transaction.getPhoneclient(), "Votre payement " + this.transaction.getTypetransaction() + " est un succès.");
            }else{
                new ControleurSms().sendSms(this.transaction.getPhoneclient(), "Votre payement " + this.transaction.getTypetransaction() + " est un échec.");
            }
                
        }
    }

}
