/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import cfcdatabase.Model.Flashpaytransaction;
import controllers.TransactionController;
import entities.Marchand;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import utilities.AppConst;
import utilities.AppUtilities;
import utilities.HttpDataResponse;
import utilities.HttpSecurity;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "InitMobileMoneyPaiementServlet", urlPatterns = {"/init-mobilemoney-paiement"})
public class InitMobileMoneyPaiementServlet extends HttpServlet {
    private static final String TAG = InitMobileMoneyPaiementServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            
            HttpSecurity.sessionAvailable(request);
            
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.EMPTY_PARAM);
            
            String telephone = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("telephone"))
                    telephone = json.getString("telephone");
                
                
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT);
            }
            
            if(telephone == null)
                throw new ValueDataException(AppConst.PARAMS_INCORRECT);
            
            Marchand marchand = (Marchand) request.getSession().getAttribute(AuthenticationServlet.MARCHAND_SESSION_KEY);
            
//            Flashpaytransaction transation = TransactionController.buildTransaction(marchand, TransactionController.TYPE_MOBILE_MONEY);
//            
//            TransactionController ctrl = new TransactionController();
//            
//            ctrl.initPaiementMobileMoney(marchand, telephone, TransactionController.TYPE_MOBILE_MONEY);
//            
//            httpResponse.setError(ctrl.getError());

            httpResponse.getError().setCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(AppConst.UNKNOWN_ERROR);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
