/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilities.HttpSecurity;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/main"})
public class MainServlet extends HttpServlet {
    private static final String TAG = MainServlet.class.getSimpleName();
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        
        try{
            HttpSecurity.sessionAvailable(request);
            HttpUtilities.sendPage(request, response, "main.jsp");
        }catch(ValueDataException e){
            request.setAttribute("message", e.getMessage());
            HttpUtilities.sendPage(request, response, "error.jsp");
        }catch(Exception e){
            e.printStackTrace();
            HttpUtilities.sendPage(request, response, "error.jsp");
        }
    }
}
