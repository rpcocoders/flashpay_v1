/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.TransactionController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import utilities.AppConst;
import utilities.AppUtilities;
import utilities.HttpDataResponse;
import utilities.HttpSecurity;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "ConfirmPaiementFlashAppServlet", urlPatterns = {"/confirm-paiement-flashpp"})
public class ConfirmPaiementFlashAppServlet extends HttpServlet {
    private static final String TAG = ConfirmPaiementFlashAppServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            HttpSecurity.sessionAvailable(request);
            
//            if(1 == 0)
//                throw new ValueDataException();
            
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.EMPTY_PARAM);
            
            String codeValidation = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("code"))
                    codeValidation = json.getString("code");
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT);
            }
            
            TransactionController ctrl = new TransactionController();
            
            String code = (String) request.getSession().getAttribute(InitFlashAppPaiementServlet.CODE_VALIDATION_KEY);
            
            if(code == null)
                throw new ValueDataException("Votre session a expiré.");
            
            if(!code.equals(codeValidation))
                throw new ValueDataException("Le code saisi est incorrect.");
            
            Long id = (Long) request.getSession().getAttribute(InitFlashAppPaiementServlet.ID_TRANSACTION_KEY);
            if(id == null)
                throw new ValueDataException("Votre session a expiré.");
            
            ctrl.confirmPaiementFlashpay(id);
            
            httpResponse.setError(ctrl.getError());
            
        }catch(ValueDataException e){
            request.getSession().invalidate();
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            request.getSession().invalidate();
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(AppConst.UNKNOWN_ERROR);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
