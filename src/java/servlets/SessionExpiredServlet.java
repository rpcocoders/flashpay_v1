/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilities.HttpUtilities;
import utilities.Logger;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "SessionExpiredServlet", urlPatterns = {"/session-expired"})
public class SessionExpiredServlet extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        HttpUtilities.sendPage(request, response, "session-expired.jsp");
    }
}
