/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import cfcdatabase.Model.Flashpaytransaction;
import controllers.TransactionController;
import entities.FlashPayTransactResponse;
import entities.Marchand;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import utilities.AppConst;
import utilities.AppUtilities;
import utilities.HttpDataResponse;
import utilities.HttpSecurity;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "InitFlashAppPaiementServlet", urlPatterns = {"/init-flashapp-paiement"})
public class InitFlashAppPaiementServlet extends HttpServlet {
    private static final String TAG = InitFlashAppPaiementServlet.class.getSimpleName();
    
    public static final String CODE_VALIDATION_KEY = "CODE_VALIDATION_KEY";
    public static final String ID_TRANSACTION_KEY = "ID_TRANSACTION_KEY";
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            HttpSecurity.sessionAvailable(request);
            
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.EMPTY_PARAM);
            
            String fpi = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("fpi"))
                    fpi = json.getString("fpi");
                
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT);
            }
            
            if(fpi == null)
                throw new ValueDataException("Aucun Flash ID envoyé");
            
            Marchand marchand = (Marchand) request.getSession().getAttribute(AuthenticationServlet.MARCHAND_SESSION_KEY);
            
            TransactionController ctrl = new TransactionController();
            
            Flashpaytransaction transaction = TransactionController.buildTransaction(marchand, TransactionController.TYPE_FLASH);
            transaction.setFlashidclient(fpi);
            
            JSONObject json = ctrl.initiatePaiement(transaction, TransactionController.TYPE_FLASH, true);
            
            if(!ctrl.getError().getCode().equals("OK"))
                throw new ValueDataException(ctrl.getError().getDescription());
            
            if(json.has("id"))
                transaction.setId(json.getLong("id"));
            
            request.setAttribute(ID_TRANSACTION_KEY, json.getLong("id"));
            
            if(json.has("codeValidation"))
                request.setAttribute(CODE_VALIDATION_KEY, json.getString("codeValidation"));
            
            httpResponse.setError(ctrl.getError());
            
        }catch(ValueDataException e){
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(e.getMessage());
        }catch(Exception e){
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(AppConst.UNKNOWN_ERROR);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
        
    }
    
    
}
