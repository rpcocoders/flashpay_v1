package servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.AuthentificationController;
import entities.GetSessionTokenResponse;
import entities.Marchand;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilities.AppConst;
import utilities.AppUtilities;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
@WebServlet(urlPatterns = {"/auth"})
public class AuthenticationServlet extends HttpServlet {

    private static final String TAG = AuthenticationServlet.class.getSimpleName();
    public static final String MARCHAND_SESSION_KEY = "marchand";
    public static final String SESSION_KEY = "session";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Logger.printLog(TAG, "debut");

        try {

//            String requestBody = HttpUtilities.getBodyRequest(request);
//            Logger.printLog(TAG, requestBody);

              if(1 == 0)
                  throw new ValueDataException();

//            AppUtilities.controlValue(requestBody, AppConst.EMPTY_PARAM);
            Marchand marchand = null;

//            try{
//                marchand = new Gson().fromJson(requestBody, new TypeToken<Marchand>(){}.getType());
//                if(marchand == null)
//                    throw new Exception("");
//            }catch(Exception e){
//                throw new ValueDataException(AppConst.PARAMS_INCORRECT);
//            }
            marchand = new Marchand();

            marchand.setLogin("lukamodric@gmail.com");
            marchand.setPwd("12345");
            marchand.setDeviseiso("USD");
            marchand.setMontant(3.0);
            marchand.setFpid("FPAA0320");
            marchand.setUrlDecline("https://www.bitbucket.org");
            marchand.setUrlSuccess("https://www.google.com");
            marchand.setTransactionId("TR-001");
            marchand.setCommentaire("Paiement venant du partenaire");
            marchand.setNomMarchand("TEST - MARCHAND");
            marchand.setToken("jnq8clocs1kh8qe9s7viub01oq");
            marchand.setPin("12345");
            marchand.setCodePin("12345");
            

            AuthentificationController ctrl = new AuthentificationController();

            GetSessionTokenResponse session = ctrl.getSession(marchand);
            
            if (!ctrl.getError().getCode().equals("OK")) {
                throw new ValueDataException(ctrl.getError().getDescription());
            }
            
//            marchand.setAgent(Long.valueOf(1));
//            marchand.setToken("XXXX");
            
            marchand.setAgent(session.getIdagent());
            marchand.setToken(session.getToken());

            request.getSession(true).setAttribute(MARCHAND_SESSION_KEY, marchand);
            //request.getSession().setAttribute(SESSION_KEY, session);

            HttpUtilities.redirect(request, response, "main");

        } catch (ValueDataException e) {
            request.setAttribute("message", e.getMessage());
            HttpUtilities.sendPage(request, response, "error.jsp");
        } catch (Exception e) {
            request.setAttribute("message", AppConst.UNKNOWN_ERROR);
            HttpUtilities.sendPage(request, response, "error.jsp");
        }
    }
}
