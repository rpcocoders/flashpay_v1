/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.TransactionController;
import entities.Marchand;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utilities.AppConst;
import utilities.HttpDataResponse;
import utilities.HttpSecurity;
import utilities.HttpUtilities;
import utilities.Logger;
import utilities.ValueDataException;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "InitPaiementVisaServlet", urlPatterns = {"/init-paiement-visa"})
public class InitPaiementVisaServlet extends HttpServlet {
    private static final String TAG = InitPaiementVisaServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            
            HttpSecurity.sessionAvailable(request);
            
            Marchand marchand = (Marchand) request.getSession().getAttribute(AuthenticationServlet.MARCHAND_SESSION_KEY);
            
            TransactionController ctrl = new TransactionController();
            
            ctrl.initiatePaiement(TransactionController.buildTransaction(marchand, TransactionController.TYPE_VISA_CARD), TransactionController.TYPE_VISA_CARD, false);
            
            
            
        }catch(ValueDataException e){
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setCode("KO");
            httpResponse.getError().setDescription(AppConst.UNKNOWN_ERROR);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
