/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author rubenpanga
 */
public class ErrorResponse {
    public static final String OK = "OK";
    public static final String KO = "KO";
    
    private String code;
    private String description;
    
    public ErrorResponse(){
        this.setCode(KO);
        this.setDescription("");
    }
    
    public ErrorResponse(String code, String description){
        this.setCode(code);
        this.setDescription(description);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}