/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author rubenpanga
 */
public class AppConst {
    public static final String EMPTY_PARAM = "Aucun paramètre reçu";
    public static final String PARAMS_INCORRECT = "paramètres incorrects";
    public static final String UNKNOWN_ERROR = "Une erreur s'est produite. Veuillez contacter le support technique";
    public static final String OPERATION_FAILED = "L'opération n'a pas abouti";
    
}
