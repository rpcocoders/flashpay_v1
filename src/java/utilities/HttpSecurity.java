/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.nio.charset.Charset;
import java.util.Base64;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rubenpanga
 */
public class HttpSecurity {

    public static void sessionAvailable(HttpServletRequest request) throws ValueDataException {
        HttpSession session = request.getSession();
        if (session.getAttribute("marchand") == null) {
            throw new ValueDataException("Votre session a expiré.");
        }
    }

    public static String[] getBasicAuthentification(HttpServletRequest request) throws ValueDataException {
        HttpSession session = request.getSession();
        String encodedCredentials = (String) session.getAttribute("user-credential");

        return HttpSecurity.getBasicAuthentification(encodedCredentials);
    }

    public static String[] getBasicAuthentification(String encodedCredentials) {
        return new String(Base64.getDecoder().decode(encodedCredentials.trim().split(" ")[1].getBytes(Charset.forName("UTF-8")))).split(":");
        //return new String (Base64.getDecoder().decode(encodedCredentials.trim().split(" ")[1].getBytes()),Charset.forName("UTF-8"));
    }
}
