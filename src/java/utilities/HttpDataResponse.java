/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author rubenpanga
 */
public class HttpDataResponse<T> {
    private ErrorResponse error;
    private T response;
    
    public HttpDataResponse(){
        this.error = new ErrorResponse();
    }
    
    public HttpDataResponse(ErrorResponse pError){
        error = pError;
    }

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
