/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import com.google.gson.Gson;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.Calendar;
import java.util.Random;
import javax.net.ssl.SSLContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 *
 * @author rubenpanga
 */
public class HttpUtilities {
    private static final String FORM_HAS_NOT_TOKEN = "Le token du formulaire n'est pas correcte.";
    private static final String FORM_TOKEN_IS_INVALIDE = "Le token du formulaire invalide.";
    
    public static String getBodyRequest(HttpServletRequest request){
        StringBuilder data = new StringBuilder();
        
        try{
            BufferedReader br =new BufferedReader(new InputStreamReader(request.getInputStream()));
            String line = "";
            while ((line = br.readLine()) != null) {
                data.append(line);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return data.toString();
    }
    
    public static ServletOutputStream prepareHttpResponse(HttpServletResponse response, HttpDataResponse dataResponse){
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        String query=new Gson().toJson(dataResponse);
        //response.addHeader("response", query);
        ServletOutputStream out=null;
        try{
            out = response.getOutputStream();
            out.write(query.getBytes("UTF-8"));
        }catch(Exception e){
            e.printStackTrace();
        }
        return out;
    }
    
    public static ServletOutputStream prepareHttpResponse(HttpServletResponse response, String query){
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        //response.addHeader("response", query);
        ServletOutputStream out=null;
        try{
            out = response.getOutputStream();
            out.write(query.getBytes("UTF-8"));
        }catch(Exception e){
            e.printStackTrace();
        }
        return out;
    }
    
    public static String generateToken(int nbr){
        String returnString = null;
        try{
            char[] alphabets = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
            StringBuilder builder = new StringBuilder();
            while(builder.toString().length()<nbr){
                builder.append(alphabets[new Random().nextInt(35)]);
            }

            builder.append(Calendar.getInstance().getTime().toString());
            returnString = Base64.getEncoder().encodeToString(builder.toString().getBytes("UTF-8"));
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return returnString;
    }
    
    public static boolean formTokenIsCorrect(HttpServletRequest request, String formCode, String token) throws ValueDataException {
        boolean succes = false;
        
        if(token==null || (token!=null && token.trim().isEmpty()))
            throw new ValueDataException("Le token qui vous a été fournit n'est pas reçu.");
        
        String tokenForm = null;
        try{
            tokenForm = (String) request.getSession().getAttribute(formCode);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        if(tokenForm==null || (tokenForm!=null && tokenForm.trim().isEmpty()))
            throw new ValueDataException(FORM_HAS_NOT_TOKEN);
        
        if(token.equals(tokenForm))
            succes = true;
        
        if(!succes)
            throw new ValueDataException(FORM_TOKEN_IS_INVALIDE);
        
        return succes;
    }
    
    public static String setFormToken(HttpServletRequest request, String formCode) throws ValueDataException {
        String token = generateToken(15);
        request.getSession().setAttribute(formCode, token);
        return token;
    }
    
    public static void emptyFormToken(HttpServletRequest request, String formCode){
        try{
            request.getSession().removeAttribute(formCode);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static void sendPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException{
        request.getRequestDispatcher("/WEB-INF/" + page).forward(request, response);
    }
    
    public static void redirect(HttpServletRequest request, HttpServletResponse response, String url) throws ServletException, IOException{
        response.sendRedirect(request.getContextPath() + "/" + url);
    }
    
    public static String readInputStream(InputStream connection) {
        String result = null;
        StringBuffer sb = new StringBuffer();
        InputStream is = null;

        try {
            is = new BufferedInputStream(connection);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }
    
    public static HttpClient createHttpClient_AcceptsUntrustedCerts(String url)
            throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        HttpClientBuilder b = HttpClientBuilder.create();

        if (url != null && url.contains("https://")) {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {

                @Override
                public boolean isTrusted(java.security.cert.X509Certificate[] xcs, String string) throws CertificateException {
                    return true;
                }
            }).build();

            b.setSslcontext(sslContext);
            SSLConnectionSocketFactory ssf = new SSLConnectionSocketFactory(sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            Registry<ConnectionSocketFactory> r = RegistryBuilder.<ConnectionSocketFactory>create().register("https", ssf).build();
            HttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(r);
            b.setConnectionManager(cm);
        }

        HttpClient client = b.build();
        return client;
    }
}
