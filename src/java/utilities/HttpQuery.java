/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 *
 * @author rubenpanga
 */
public class HttpQuery {

    public static HttpURLConnection postRequest(String application, String applicationKey, String urlString, String request) {
        HttpURLConnection connection = null;
        DataOutputStream daos = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(60000);
            connection.setConnectTimeout(60000);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/json" + ", charset=utf8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Encoding", "UTF8");

            connection.setRequestProperty("Authorization", getBasicAuthorization(application, applicationKey));

            if (request != null) {
                connection.setRequestProperty("data", request);
            }
            connection.connect();
            if (connection != null) {
                daos = new DataOutputStream(connection.getOutputStream());
            }

            if (daos != null) {
                if (request != null) {
                    daos.writeBytes(request);
                }
                daos.close();
                daos.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //if(connection!=null)
            //connection.disconnect();
        }

        return connection;
    }

    public static HttpURLConnection postRequest(String urlString, String request) throws ConnectException {
        HttpURLConnection connection = null;
        DataOutputStream daos = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(60000);
            connection.setConnectTimeout(60000);
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/json" + ", charset=utf8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Encoding", "UTF8");

            //connection.setRequestProperty("Authorization", getBasicAuthorization(application, applicationKey));
            if (request != null) {
                connection.setRequestProperty("data", request);
            }
            connection.connect();
            if (connection != null) {
                daos = new DataOutputStream(connection.getOutputStream());
            }

            if (daos != null) {
                if (request != null) {
                    daos.writeBytes(request);
                }
                daos.close();
                daos.flush();
            }
        } catch (ConnectException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //if(connection!=null)
            //connection.disconnect();
        }

        return connection;
    }

    public static String getBasicAuthorization(String application, String applicationKey) {
        String encodedAuthentification = "";
        try {
            encodedAuthentification = "Basic " + Base64.getEncoder().encodeToString((application + ":" + applicationKey).getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encodedAuthentification;
    }

    public static String getResponse(HttpURLConnection connection) {
        StringBuilder outputResponse = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = "";
            while ((line = br.readLine()) != null) {
                outputResponse.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputResponse.toString();
    }

    public static HttpURLConnection postRequest(boolean isGetMethod, String urlString, String request) {
        HttpURLConnection connection = null;
        DataOutputStream daos = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(60000);
            connection.setConnectTimeout(60000);
            connection.setRequestMethod((isGetMethod ? "GET" : "POST"));
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/json" + ", charset=utf8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Encoding", "UTF8");

            if (request != null) {
                connection.setRequestProperty("data", request);
            }

            connection.connect();
            if (connection != null) {
                daos = new DataOutputStream(connection.getOutputStream());
            }

            if (daos != null) {
                if (request != null) {
                    daos.writeBytes(request);
                }
                daos.close();
                daos.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //if(connection!=null)
            //connection.disconnect();
        }

        return connection;
    }

    public static HttpURLConnection sendByGETMethod(String urlString, boolean https) {
        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(urlString); 

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setInstanceFollowRedirects(false);
            /* added line */

            InputStream in = urlConnection.getInputStream();

            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                System.out.print(current);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        
        return urlConnection;
    }

    public static HttpURLConnection getRequest(String urlString, boolean https) {
        HttpURLConnection con = null;

        try {

            URL url = new URL(urlString);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
            //con.setDoOutput(true);
            //con.setChunkedStreamingMode(0);

            //OutputStream out = new BufferedOutputStream(con.getOutputStream());
            InputStream in = new BufferedInputStream(con.getInputStream());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }

        return con;
    }
}
