/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author rubenpanga
 */
public class Logger {
    public static final String PREFIX_TAG = "----------------- ";
    
    public static void printLog(String TAG, String message){
        System.out.println(new SimpleDateFormat("dd/MM/YYYY HH:MM").format(Calendar.getInstance().getTime()) + " ---> " + PREFIX_TAG + TAG + " ; " + message);
    }
}
