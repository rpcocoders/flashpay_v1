/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author rubenpanga
 */
public class QueryParam {
    private String mAttribut;
    private Object mValue;

    public QueryParam() {

    }

    public QueryParam(String pAttribut, Object pValue) {
        this.mAttribut = pAttribut;
        this.mValue = pValue;
    }

    public String getAttribut() {
        return mAttribut;
    }

    public void setAttribut(String col) {
        this.mAttribut = col;
    }

    public Object getValue() {
        return mValue;
    }

    public void setValue(Object value) {
        this.mValue = value;
    }
}
