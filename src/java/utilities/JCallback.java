/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author rubenpanga
 */
public interface JCallback {
    void onSuccessed(Object object);
    void onFailed(Object object);
    void onDelete(boolean responseState, Object object);
    void onCancel();
    void pushMessage(String message);
}
