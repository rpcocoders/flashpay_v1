/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.security.Security;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;

/**
 *
 * @author rubenpanga
 */
public class MailUtility {

    public static final String ADRESSE_ECOLLECT_NO_REPLY = "ceboma@trinix.cd";
    public static final String PASSWORD_ECOLLECT_NO_REPLY = "monzotoec**13051984";

    public static Address[] getRecipients(String listStr) throws AddressException {
        ArrayList<InternetAddress> list = new ArrayList<InternetAddress>();
        String[] emailList = listStr.split(";");
        for (String email : emailList) {
            list.add(new InternetAddress(email));
        }
        return (Address[]) list.toArray(new InternetAddress[list.size()]);
    }

    public static Address[] getRecipients(List<String> listStr) throws AddressException {
        ArrayList<InternetAddress> list = new ArrayList<InternetAddress>();
        for (String email : listStr) {
            list.add(new InternetAddress(email));
        }
        return (Address[]) list.toArray(new InternetAddress[list.size()]);
    }

    public MailUtility() {
    }

    public void processMail(String senderEmail, String password, String subject, String textMsg, List<String> recipients)
            throws NoSuchProviderException, AddressException, MessagingException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sendMail(senderEmail, password, subject, textMsg, recipients);
                } catch (AddressException ex) {
                    ex.printStackTrace();
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    public void sendMail(String senderEmail, String password, String subject, String textMsg, List<String> recipients)
            throws NoSuchProviderException, AddressException, MessagingException {
        sendMail(senderEmail, password, subject, textMsg, recipients, null, null);
    }

    public void sendMail(String senderEmail, String password, String subject, String textMsg, List<String> recipients, String filePath, String fileName)
            throws NoSuchProviderException, AddressException, MessagingException {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");

        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.ssl.enable", "true");
//props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        Session session2 = Session.getInstance(props, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderEmail, password);
            }
        });

        session2.setDebug(true);

        Transport transport = session2.getTransport();
        InternetAddress addressFrom = new InternetAddress(senderEmail);
        props.put("mail.smtp.user", addressFrom);
        props.put("mail.smtp.password", password);

        MimeMessage message = new MimeMessage(session2);
        message.setSender(addressFrom);
        message.setSubject(subject);

        Multipart multipart = new MimeMultipart();

        BodyPart messageBodyPartText = new MimeBodyPart();
        messageBodyPartText.setText(textMsg);
        multipart.addBodyPart(messageBodyPartText);
        if (filePath != null) {
            BodyPart messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(filePath);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);
        }

        message.setContent(multipart, "text/plain");

        Address[] to = MailUtility.getRecipients(recipients);
        message.addRecipients(Message.RecipientType.TO, to);
        transport.connect();
        transport.send(message);
        transport.close();

    }

    public void processBulkMail(String senderEmail, String password, String subject, List<String[]> emailTxtMap)
            throws NoSuchProviderException, AddressException, MessagingException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    for (String[] entry : emailTxtMap) {
                        String textMsg = entry[1];
                        List<String> recipients = new ArrayList<String>();
                        recipients.add(entry[0]);

                        sendMail(senderEmail, password, subject, textMsg, recipients);
                    }

                } catch (AddressException ex) {
                    ex.printStackTrace();
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    public void processMail1(String senderEmail, String password, String subject, String textMsg, List<String> recipients, String contentType, String base64)
            throws NoSuchProviderException, AddressException, MessagingException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sendMail1(senderEmail, password, subject, textMsg, recipients, false, contentType, base64);
                } catch (AddressException ex) {
                    ex.printStackTrace();
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    public void sendMail1(String senderEmail, String password, String subject, String textMsg, List<String> recipients,
             boolean isPlainText, String contentType, String base64)
            throws NoSuchProviderException, AddressException, MessagingException {

        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");

        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.ssl.enable", "true");
//props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        Session session2 = Session.getInstance(props, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderEmail, password);
            }
        });

        session2.setDebug(true);

        Transport transport = session2.getTransport();
        InternetAddress addressFrom = new InternetAddress(senderEmail);
        props.put("mail.smtp.user", addressFrom);
        props.put("mail.smtp.password", password);

        MimeMessage message = new MimeMessage(session2);
        message.setSender(addressFrom);
        message.setSubject(subject);

        Multipart multipart = new MimeMultipart();

        BodyPart messageBodyPart = new MimeBodyPart();
        if (isPlainText) {
            messageBodyPart.setText(textMsg);
            multipart.addBodyPart(messageBodyPart);
        } else {
            messageBodyPart.setContent(textMsg, contentType);
            multipart.addBodyPart(messageBodyPart);
            PreencodedMimeBodyPart pmp = new PreencodedMimeBodyPart("base64");
            pmp.setDisposition(MimeBodyPart.INLINE);
            pmp.setHeader("Content-ID", "<idimg>");
            pmp.setContent(base64, "image/png");
            multipart.addBodyPart(pmp);
        }

        message.setContent(multipart, contentType);

        Address[] to = MailUtility.getRecipients(recipients);
        message.addRecipients(Message.RecipientType.TO, to);
        transport.connect();
        transport.send(message);
        transport.close();

    }
}
