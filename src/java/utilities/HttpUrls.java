/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author rubenpanga
 */
public class HttpUrls {
    private static final String IP = "192.168.202.249";
    private static final String PORT = "3000";
    private static final String SCHEMAS = "http";
    
    
    private static final String getBase(){
        return SCHEMAS + "://" + IP + ":" + PORT + "/";
    }
    
    public static final String API_GET_SESSION_TOKEN = getBase() + "api/services/get/token";
    public static final String API_INITIATE_TRANSACTION = getBase() + "api/FlashPayTransactServlet";
    public static final String API_CONFIRM_TRANSACTION = getBase() + "api/FlashPayTransactConfirmServlet";
    public static final String API_RETRAIT_MOBILE_MONEY = getBase() + "api/RetraitMobileMoney";
    public static final String API_CHECK_STATUS_MOBILE_MONEY = getBase() + "api/RetraitMobileMoneyStatutServlet";
    
    
}
