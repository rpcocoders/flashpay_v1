/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;

/**
 *
 * @author christianeboma
 */
public class AppUtilities {

    public static void controlValue(List list, String message) throws ValueDataException {
        if (list == null || (list != null && list.isEmpty())) {
            throw new ValueDataException(message);
        }
    }

    public static void controlValue(Object[] list, String message) throws ValueDataException {
        if (list == null || (list != null && list.length <= 0)) {
            throw new ValueDataException(message);
        }
    }

    public static void controlValue(Double d, String message) throws ValueDataException {
        if (d == null || (d != null && d <= 0)) {
            throw new ValueDataException(message);
        }
    }

    public static void controlValue(String s, String message) throws ValueDataException {
        if (s == null || (s != null && s.trim().isEmpty())) {
            throw new ValueDataException(message);
        }
    }

    public static void controlValue(Integer value, String message) throws ValueDataException {
        if (value == null || (value != null && value <= 0)) {
            throw new ValueDataException(message);
        }
    }
    
    public static void controlValue(Long value, String message) throws ValueDataException {
        if (value == null || (value != null && value <= 0)) {
            throw new ValueDataException(message);
        }
    }

    public static void controlValue(String message, Double... values) throws ValueDataException {
        if (values == null || (values != null && values.length == 0)) {
            throw new ValueDataException(message);
        }

        for (Double d : values) {
            if (d == null || (d != null && d <= 0)) {
                throw new ValueDataException(message);
            }
        }
    }

    public static void controlValue(String message, String... values) throws ValueDataException {
        if (values == null || (values != null && values.length == 0)) {
            throw new ValueDataException(message);
        }

        for (String s : values) {
            if (s == null || (s != null && s.trim().isEmpty())) {
                throw new ValueDataException(message);
            }
        }
    }

    public static void controlValue(String message, Integer... values) throws ValueDataException {
        if (values == null || (values != null && values.length == 0)) {
            throw new ValueDataException(message);
        }

        for (Integer value : values) {
            if (value == null || (value != null && value <= 0)) {
                throw new ValueDataException(message);
            }
        }
    }

    public static Calendar getDateForFrancJour(int addDay, Date debut) {

        if (debut == null) {
            debut = Calendar.getInstance().getTime();
        }

        List<Calendar> dates = new ArrayList<Calendar>();

        int dayActual = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        for (int i = 0; i < addDay; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(debut);
            calendar.add(Calendar.DAY_OF_MONTH, i);

            dates.add(calendar);
        }

        int holiday = 0;

        for (int i = dates.size() - 1; i > 0; i--) {
            Calendar date = dates.get(i);
            if (date.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || date.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                holiday++;
                //System.out.println("Date : " + new SimpleDateFormat("dd-MM-yyyy").format(date.getTime()) + " : WEEKEND - " + date.get(Calendar.DAY_OF_WEEK));

                dates.remove(i);
            } else {
                //System.out.println("Date : " + new SimpleDateFormat("dd-MM-yyyy").format(date.getTime()));
            }
        }

        if (holiday > 0) {
            Calendar last = dates.get(dates.size() - 1);
            last.add(Calendar.DAY_OF_MONTH, 1);
            dates.add(getDateForFrancJour(holiday, last.getTime()));
        }

        return dates.get(dates.size() - 1);
    }

    public static Calendar getDateForJour(int addDay, Date debut) throws ValueDataException {

        if (debut == null) {
            throw new ValueDataException("date non renseignée.");
        }

        Calendar date = Calendar.getInstance();
        date.setTime(debut);

        date.add(Calendar.DAY_OF_MONTH, addDay);

        return date;
    }

    public static String convertStringDateToYyyyMMdd(String s) throws ValueDataException {

        try {
            s = escapeSpecialChar(s);
            int occurrence = s.indexOf("/");
            if (occurrence > 0) {
                String dd = "", mm = "", yyyy = "";

                if (occurrence == 2) {
                    //La date commence par le jour
                    dd = s.substring(0, occurrence);
                    mm = s.substring(3, 5);
                    yyyy = s.substring(6);

                } else if (occurrence == 4) {
                    //La date commence par l'annéé
                    yyyy = s.substring(0, occurrence);
                    mm = s.substring(5, 7);
                    dd = s.substring(8);
                }

                s = yyyy + mm + dd;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ValueDataException("Le format de la date n'est pas correcte.");
        }

        controlValue(s, "Le format de la date n'est pas correct.");

        return s;
    }

    public static String escapeSpecialChar(String s) {

        try {
            controlValue(s, "Valeur nulle");

            return s.trim().replace(".", "/")
                    .replace("/", "/")
                    .replace("-", "/")
                    .replace("*", "/")
                    .replace("+", "/")
                    .replace(",", "/");

        } catch (ValueDataException e) {

        }

        return "";

    }

    public static String convertToString(byte[] bytes) {
        String result = null;
        try {
            result = new String(bytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getStringFormat(Date date) {
        String returnDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        returnDate = sdf.format(date);
        return returnDate;
    }

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String randomAlphaNumeric(int count) {

        StringBuilder builder = new StringBuilder();

        while (count-- != 0) {

            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());

            builder.append(ALPHA_NUMERIC_STRING.charAt(character));

        }

        return builder.toString();

    }

    private static DecimalFormat df = null;

    public static String convertNumber(Double montant) {
        String re = "0";
        if (montant != null) {
            if (df == null) {
                Locale locale = new Locale("fr", "FR");
                DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
                symbols.setDecimalSeparator(',');
                symbols.setGroupingSeparator(' ');

                df = new DecimalFormat("###,###,###,###,###.##", symbols);

                df.setGroupingSize(3);
                df.setGroupingUsed(true);

            }
            re = df.format(montant);
        }
        return re;
    }

    public static double round(double number, int pos) {
        BigDecimal bd = new BigDecimal(String.valueOf(number));
        bd = bd.setScale(2, BigDecimal.ROUND_FLOOR);
        return bd.doubleValue();
    }

    public static String encode(String string) {
        return Base64.getEncoder().encodeToString(string.getBytes(StandardCharsets.UTF_8));
    }

    public static String decode(String string) {
        byte[] decodedBytes = Base64.getDecoder().decode(string);
        return new String(decodedBytes, StandardCharsets.UTF_8);
    }

    public static String convertNumber(Integer montant) {
        String re = "0";
        if (montant != null) {
            if (df == null) {
                Locale locale = new Locale("fr", "FR");
                DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);

                df = new DecimalFormat("###,###,###,###,###", symbols);

                df.setGroupingSize(3);
                df.setGroupingUsed(true);

            }
            re = df.format(montant);
        }
        return re;
    }

    public static String wrapText(String text, int maxCharacter) {
        StringBuilder rtn = new StringBuilder();

        if (text != null && text.length() > maxCharacter) {
            text = text.trim();

            String[] words = text.split(" ");
            int line = 0;

            for (int i = 0; i < words.length; i++) {
                line += words[i].length();

                if (line > maxCharacter) {
                    rtn.append("\n");
                    line = 0;
                }

                rtn.append(" " + words[i]);
            }
        } else if (text != null && text.length() < maxCharacter) {
            rtn.append(text);
        }

        return rtn.toString();
    }

    public static byte[] convert(Image obj) {
        int w = (int) obj.getWidth();
        int h = (int) obj.getHeight();

        // Create a new Byte Buffer, but we'll use BGRA (1 byte for each channel) //
        byte[] buf = new byte[w * h * 4];

        /* Since you can get the output in whatever format with a WritablePixelFormat,
                       we'll use an already created one for ease-of-use. */
        obj.getPixelReader().getPixels(0, 0, w, h, PixelFormat.getByteBgraInstance(), buf, 0, w * 4);

        return buf;
    }

    public static String convert(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static byte[] convert(String string) {
        return Base64.getDecoder().decode(string);
    }

    public static LocalDate convertDateFromLocalDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = null;

        try {
            localDate = LocalDate.parse(date, formatter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return localDate;
    }

    public static String convertDateFromString(LocalDate date) {
        String string = null;
        try {
            string = date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return string;
    }

    public static String convertDate(Date dateNaissance) {
        return new SimpleDateFormat("dd/MM/yyyy").format(dateNaissance);
    }

    public static String convertDateYyyyMMdd(Date date) {
        return new SimpleDateFormat("yyyyMMdd").format(date);
    }

    public static String convertPhoneToInternationAnnotation(String prefix, String phone, String message) throws ValueDataException {

        phone = phone.trim().replace("+", "");
        if (phone.startsWith("00")) {
            phone = phone.substring(2, phone.length() - 1);
        }

        if (phone.startsWith("0")) {
            phone = prefix + phone.substring(1, phone.length());
        }

        if (phone.isEmpty()) {
            throw new ValueDataException(message);
        }

        return phone;
    }

    public static double roundToUP(double value, int precision) {
        String s = "";

        for (int i = 0; i < precision; i++) {
            s += "#";
        }

        DecimalFormat df = new DecimalFormat("#." + s);
        df.setRoundingMode(RoundingMode.CEILING);

        return Double.valueOf(df.format(value));
    }

    private static String unite(int nombre) {
        String unite = "";
        switch (nombre) {
            case 0:
                unite = "zéro";
                break;
            case 1:
                unite = "un";
                break;
            case 2:
                unite = "deux";
                break;
            case 3:
                unite = "trois";
                break;
            case 4:
                unite = "quatre";
                break;
            case 5:
                unite = "cinq";
                break;
            case 6:
                unite = "six";
                break;
            case 7:
                unite = "sept";
                break;
            case 8:
                unite = "huit";
                break;
            case 9:
                unite = "neuf";
                break;
        }
        return unite;
    }

    private static String dizaine(int nombre) {
        String dizaine = "";
        switch (nombre) {
            case 10:
                dizaine = "dix";
                break;
            case 11:
                dizaine = "onze";
                break;
            case 12:
                dizaine = "douze";
                break;
            case 13:
                dizaine = "treize";
                break;
            case 14:
                dizaine = "quatorze";
                break;
            case 15:
                dizaine = "quinze";
                break;
            case 16:
                dizaine = "seize";
                break;
            case 17:
                dizaine = "dix-sept";
                break;
            case 18:
                dizaine = "dix-huit";
                break;
            case 19:
                dizaine = "dix-neuf";
                break;
            case 20:
                dizaine = "vingt";
                break;
            case 30:
                dizaine = "trente";
                break;
            case 40:
                dizaine = "quarante";
                break;
            case 50:
                dizaine = "cinquante";
                break;
            case 60:
                dizaine = "soixante";
                break;
            case 70:
                dizaine = "soixante-dix";
                break;
            case 80:
                dizaine = "quatre-vingt";
                break;
            case 90:
                dizaine = "quatre-vingt-dix";
                break;
        }

        return dizaine;
    }

    public static String convertNumberToLetterV2(long nombre) {

        double nb;
        double i, j, quotient;
        int n = 0, reste = 0;

        String numberToLetter = "";
        //__________________________________

        if (String.valueOf(nombre).length() > 15) {
            return "dépassement de capacité";
        }

        nb = nombre;
        if (Math.ceil(nb) != nb) {
            return "Nombre avec virgule non géré.";
        }

        n = String.valueOf(nombre).length();
        switch (n) {
            case 1:
                numberToLetter = unite((int) nb);
                break;
            case 2:
                if (nb > 19) {
                    quotient = Math.floor(nb / 10);
                    reste = (int) nb % 10;
                    if (nb < 71 || (nb > 79 && nb < 91)) {
                        if (reste == 0) {
                            numberToLetter = dizaine((int) quotient * 10);
                        }
                        if (reste == 1) {
                            numberToLetter = dizaine((int) quotient * 10) + "-et-" + unite(reste);
                        }
                        if (reste > 1) {
                            numberToLetter = dizaine((int) quotient * 10) + "-" + unite(reste);
                        }
                    } else {
                        numberToLetter = dizaine((int) (quotient - 1) * 10) + "-" + dizaine(10 + reste);
                    }
                } else {
                    numberToLetter = dizaine((int) nb);
                }
                break;
            case 3:
                quotient = Math.floor(nb / 100);
                reste = (int) nb % 100;
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "cent";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "cent" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = unite((int) quotient) + " cents";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = unite((int) quotient) + " cent " + convertNumberToLetterV2(reste);
                }
                break;
            case 4:
                quotient = Math.floor(nb / 1000);
                reste = (int) (nb - quotient * 1000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "mille";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "mille" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " mille";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " mille " + convertNumberToLetterV2(reste);
                }
                break;
            case 5:
                quotient = Math.floor(nb / 1000);
                reste = (int) (nb - quotient * 1000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "mille";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "mille" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " mille";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " mille " + convertNumberToLetterV2(reste);
                }
                break;
            case 6:
                quotient = Math.floor(nb / 1000);
                reste = (int) (nb - quotient * 1000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "mille";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "mille" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " mille";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " mille " + convertNumberToLetterV2(reste);
                }
                break;
            case 7:
                quotient = Math.floor(nb / 1000000);
                reste = (int) (nb % 1000000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "un million";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "un million" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " millions";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " millions " + convertNumberToLetterV2(reste);
                }
                break;
            case 8:
                quotient = Math.floor(nb / 1000000);
                reste = (int) (nb % 1000000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "un million";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "un million" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " millions";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " millions " + convertNumberToLetterV2(reste);
                }
                break;
            case 9:
                quotient = Math.floor(nb / 1000000);
                reste = (int) (nb % 1000000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "un million";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "un million" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " millions";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " millions " + convertNumberToLetterV2(reste);
                }
                break;
            case 10:
                quotient = Math.floor(nb / 1000000000);
                reste = (int) (nb - quotient * 1000000000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "un milliard";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "un milliard" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " milliards";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " milliards " + convertNumberToLetterV2(reste);
                }
                break;
            case 11:
                quotient = Math.floor(nb / 1000000000);
                reste = (int) (nb - quotient * 1000000000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "un milliard";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "un milliard" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " milliards";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " milliards " + convertNumberToLetterV2(reste);
                }
                break;
            case 12:
                quotient = Math.floor(nb / 1000000000);
                reste = (int) (nb - quotient * 1000000000);
                if (quotient == 1 && reste == 0) {
                    numberToLetter = "un milliard";
                }
                if (quotient == 1 && reste != 0) {
                    numberToLetter = "un milliard" + " " + convertNumberToLetterV2(reste);
                }
                if (quotient > 1 && reste == 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " milliards";
                }
                if (quotient > 1 && reste != 0) {
                    numberToLetter = convertNumberToLetterV2((int) quotient) + " milliards " + convertNumberToLetterV2(reste);
                }
                break;
            /*case 13: quotient = Math.floor(nb / 1000000000000);
                reste = (int) (nb - quotient * 1000000000000);
                if (quotient == 1 && reste == 0) numberToLetter = "un billion";
                if (quotient == 1 && reste != 0) numberToLetter = "un billion" + " " + convertNumberToLetterV2((double) reste);
                if (quotient > 1 && reste == 0) numberToLetter = convertNumberToLetterV2(quotient) + " billions";
                if (quotient > 1 && reste != 0) numberToLetter = convertNumberToLetterV2(quotient) + " billions " + convertNumberToLetterV2((double) reste);
                break;
            case 14: quotient = Math.floor(nb / 1000000000000);
                reste = nb - quotient * 1000000000000;
                if (quotient == 1 && reste == 0) numberToLetter = "un billion";
                if (quotient == 1 && reste != 0) numberToLetter = "un billion" + " " + this.NumberToLetter(reste);
                if (quotient > 1 && reste == 0) numberToLetter = this.NumberToLetter(quotient) + " billions";
                if (quotient > 1 && reste != 0) numberToLetter = this.NumberToLetter(quotient) + " billions " + this.NumberToLetter(reste);
                break;
            case 15: quotient = Math.floor(nb / 1000000000000);
                reste = nb - quotient * 1000000000000;
                if (quotient == 1 && reste == 0) numberToLetter = "un billion";
                if (quotient == 1 && reste != 0) numberToLetter = "un billion" + " " + this.NumberToLetter(reste);
                if (quotient > 1 && reste == 0) numberToLetter = this.NumberToLetter(quotient) + " billions";
                if (quotient > 1 && reste != 0) numberToLetter = this.NumberToLetter(quotient) + " billions " + this.NumberToLetter(reste);
                break;
             */
        }

        /*if (numberToLetter.substring(numberToLetter.length() - "quatre-vingt".length()-1, "quatre-vingt".length()-1) == "quatre-vingt") 
            numberToLetter = numberToLetter + "s";
        */
        return numberToLetter;
    }
    
    private static final SecureRandom secureRandom = new SecureRandom(); //threadsafe
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder(); //threadsafe

    public static String generateNewToken() {
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }
}
