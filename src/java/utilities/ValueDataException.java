/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author rubenpanga
 */
public class ValueDataException extends Exception{
    public ValueDataException(){
        super();
    }
    
    public ValueDataException(String message){
        super(message);
    }
    
    public ValueDataException(Throwable cause, String message){
        super(message, cause);
    }
}