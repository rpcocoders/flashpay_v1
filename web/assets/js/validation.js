

/*
 * Fonction qui permet de verifier la qualité du mot de passe inseré par l'utilisateur.
 * Elle reçoit en paramettre le mot de passe et l'id de la balise où afficher les messages de check.
 */
function checkpassw(mdp, idBaliseCheckMdp){
        var len=mdp.length ;
	if(len<3){
		document.getElementById(idBaliseCheckMdp).innerHTML="Trop court (Min 3 caratères)";
		document.getElementById(idBaliseCheckMdp).style.color="red" ;
		document.getElementById(idBaliseCheckMdp).focus();
		return false;
	}else if(len <8){
		document.getElementById(idBaliseCheckMdp).innerHTML="Faible";
		document.getElementById(idBaliseCheckMdp).style.color="Yellow";
		return true ;
	}else{
                
		document.getElementById(idBaliseCheckMdp).innerHTML="Fort" ;
		document.getElementById(idBaliseCheckMdp).style.color="cyan";
		return true ;
	}
}

/*
 * Fonction qui permet de verifier la structure de l'adresse email.
 * Elle reçoit en paramettre 
 */
function checkemail(mail , idBaliseCheckEmail){
	//var str=document.regform.email.value
        var str=mail;
	var len=str.length
        if(len>1)
        {
            var iat=str.indexOf("@");
            var idot=str.indexOf(".");
            var idot2=str.indexOf(".",iat);
            if(iat<1||(iat>idot2)|| idot2>=len-1){
                    document.getElementById(idBaliseCheckEmail).innerHTML="Email invalide";
                    document.getElementById(idBaliseCheckEmail).style.color="red" ;
                    document.getElementById(idBaliseCheckEmail).focus();
                    return false;
            }else{
                    document.getElementById(idBaliseCheckEmail).innerHTML="Email ok";
                    document.getElementById(idBaliseCheckEmail).style.color="green";
                    return true;
            }
        }
        document.getElementById(idBaliseCheckEmail).innerHTML="Email invalide";
        document.getElementById(idBaliseCheckEmail).style.color="red" ;
        return false;
}

/*
 * Fonction qui permet de verifier la structure de l'adresse email.
 */
function isValidEmail(id){
	var el = document.getElementById(id);
        var str=el.value;
	var len=str.length;
        if(len>1)
        {
            var iat=str.indexOf("@");
            var idot=str.indexOf(".");
            var idot2=str.indexOf(".",iat);
            if(iat<1||(iat>idot2)|| idot2>=len-1){
                    el.className = 'input_design_incorrect';
                    return false;
            }else{
                    el.className = 'input_design';
                    return true;
            }
        }
        el.className = 'input_design_incorrect';
        return false;
}
/*
 * Fonction qui permet de verifier la structure de l'adresse email. Si la valeur est vide, retourne 'true'
 */
function isValidEmailOrEmpty(id){
	var el = document.getElementById(id);
        var str=el.value;
	var len=str.length;
        if(len>1)
        {
            var iat=str.indexOf("@");
            var idot=str.indexOf(".");
            var idot2=str.indexOf(".",iat);
            if(iat<1||(iat>idot2)|| idot2>=len-1){
                    el.className = 'input_design_incorrect';
                    return false;
            }else{
                    el.className = 'input_design';
                    return true;
            }
        }
        
        return true;
}

/*
 * Fonction qui permet de verifier si un caractere est un nombre.
 */
function isNumber(char){
	var numcheck = /\D/;
	for(i=0;i<char.length;i++){
		if(numcheck.test(char.charAt(i)))
			return false;
	}	
	return true;
}
/*
 * Fonction qui permet de verifier si un caractere est un nombre.
 */
function isNumberDecimal(char){	
    char = char.replace(",", ".");
    return !isNaN(parseFloat(char)) && isFinite(char);
}

/*Remplacer ',' par '.' pour les caluls*/
function getEnglishDecimal(num){
    num = num+'';
    var numStr = '';
    var numsArray = num.split(" ");
    for(var i=0;i<numsArray.length;i++){
        numStr += numsArray[i];
    }
    return numStr.replace(",", ".");
}

/*Remplacer '.' par ',' pour les caluls*/
function getFrenchDecimal(num){	
    num = num+'';
    return num.replace(".", ",");
}

/*
 *Fonction qui permet de verifier la validiter d'un numero de phone.
 * Elle reçoit en paramettre le numero de phone et l'id de la balise où afficher les messages de check.
 */
function checkphno(num , idCheckPhone){	
	//var len=document.regform.phno.value.length
        var len=num.length;
		if(len==0){
                        if(idCheckPhone != null && idCheckPhone != undefined)
			document.getElementById(idCheckPhone).innerHTML="Num phone vide";
			return false;
		}
		else if(isNumber(num)){
			if(len>9&&len<13){
                            if(idCheckPhone != null && idCheckPhone != undefined){
				document.getElementById(idCheckPhone).innerHTML="Phone No OK";
				document.getElementById(idCheckPhone).style.color="green" ;
                            }
				return true;
			}else{
                            if(idCheckPhone != null && idCheckPhone != undefined){
				document.getElementById(idCheckPhone).innerHTML="Numéro incorrect ";
				document.getElementById(idCheckPhone).style.color="red" ;
                            }
			}
		}else{
                    if(idCheckPhone != null && idCheckPhone != undefined){
			document.getElementById(idCheckPhone).innerHTML="Numéro incorrect";
			document.getElementById(idCheckPhone).style.color="red" ;
                    }
			}	
	return false;
}

/*
 * Fonction qui permet de verifier le format d'heure inserée par l'utilisateur
 */
function checkTime(time)
{
    var Ttime =time.split(":");
    if(Ttime.length == 2)
    {
        var heure = Ttime[0];
        var minut = Ttime[1];
        if((heure*1>-1 && heure*1<24) && (minut*1>-1  && minut*1<60))
        {
            return true;
        }
        
        showDialog("Format d'heure HH:MM");
        return false;
    }
    else
    {
        
        showDialog("Format d'heure HH:MM");
        return false;
    }
}
/*
 * Verifier que le string n'est pas vide
 */
function isNotEmpty(str){
    
    if(str !== undefined && str.trim().length > 0 && str.trim() != ''){
        return true;
    }
    return false;
}

/* Verifier si la valeur (Texte) de l'element est vide*/
function isElementTextNotEmpty(id){
    return isElementTextNotEmpty1(id,true);
}
function isElementTextNotEmpty1(id,isChangeclass){
    
    var el = document.getElementById(id);
    var str = el.value;
    if(str !== undefined && str.trim().length > 0 && str.trim() != ''){
        el.className = 'input_design';
        return true;
    }
    if(isChangeclass)
        el.className = 'input_design_incorrect shake_anim';
    return false;
}
/* Verifier si un element a été sélectionné dans un menu deroulant*/
function isSelectElementSeleted(id){
    return isSelectElementSeleted1(id,true);
}
function isSelectElementSeleted1(id,isChangeclass){
    
    var el = document.getElementById(id);
    var str = el.value;
   
    if(str !== undefined && str != '-1' && str.trim() != ''){
        el.className = 'input_design';
        return true;
    }
    if(isChangeclass)
        el.className = 'input_design_incorrect shake_anim';
    return false;
}

//verifier la value de 'jour' , 'mois' ou 'année' entrée par l'utilisateur
function checkDateField(id){
    var value = document.getElementById(id).value;
    var date = new Date();
    
    if(id == 'dd'){
        if(!isNumber(value) || value > 31){
            showDialog('Donnée invalid!');
            document.getElementById(id).value = date.getDate();
        }
    }
    else if(id == 'mm'){
        if(!isNumber(value) || value > 12){
            showDialog('Donnée invalid!');
            document.getElementById(id).value = date.getMonth();
        }
    }
    else if(id == 'aaaa'){
        if(!isNumber(value) ){
            showDialog('Donnée invalid!');
            document.getElementById(id).value = date.getFullYear();
        }
    }   
}
function isATextAlpha(id)
{
    var el = document.getElementById(id);
    var text = el.value;
    var numcheck = /[a-z]/i;
    if(text.length<1){
        el.className = 'input_design_incorrect';
                return false;
    }
    for(i=0;i<text.length;i++){
            if(!numcheck.test(text.charAt(i)) && text.charAt(i)!=" " && text.charAt(i)!="-")
            {
                el.className = 'input_design_incorrect';
                return false;
            } 
    }
    el.className = 'input_design';
    return true;
}
function isATextAlphaOrEmpty(id)
{
    var el = document.getElementById(id);
    var text = el.value;
    var numcheck = /[a-z]/i;
    if(text.length<1){
                return true;
    }
    for(i=0;i<text.length;i++){
            if(!numcheck.test(text.charAt(i)) && text.charAt(i)!=" ")
            {
                el.className = 'input_design_incorrect';
                return false;
            } 
    }
    el.className = 'input_design';
    return true;
}
function isATextAlphaNumeric(id)
{
    var el = document.getElementById(id);
    var text = el.value;
    var numcheck = /^[a-z0-9]+$/i;
    if(text.length<1){
        el.className = 'input_design_incorrect';
        return false;
    }
    for(i=0;i<text.length;i++){
            if(!numcheck.test(text.charAt(i)) && text.charAt(i)!=" ")
            {
                    el.className = 'input_design_incorrect';
                    return false;
            } 
    }
    el.className = 'input_design';
    return true;
}

function isNumberPhone(char){
	var numcheck = /\D/;
	for(i=0;i<char.length;i++){
		if(numcheck.test(char.charAt(i)) && char.charAt(i)!="+")
			return false;
	}	
	return true;
}
function isElementNumberPhone(id){
    return isElementNumberPhoneOrEmpty(id, false);
}
function isElementNumberPhoneOrEmpty(id, isOrEmpty){
	var numcheck = /\D/;
        var el = document.getElementById(id);
        var char = el.value;
        if(isOrEmpty && char.trim().length == 0)
            return true;
        if(char.length<9){
            el.className = 'input_design_incorrect';
            return false;
        }
        
	for(i=0;i<char.length;i++){
		if(numcheck.test(char.charAt(i)) && char.charAt(i)!="+"){
                        el.className = 'input_design_incorrect';
			return false;
                    }
	}
        el.className = 'input_design';
	return true;
}
/*
 * Fonction qui permet de verifier la structure de l'adresse email.
 * Elle reçoit en paramettre 
 */
function checkemail(mail , idBaliseCheckEmail){
	//var str=document.regform.email.value
        var str=mail;
	var len=str.length
        if(len>1)
        {
            var iat=str.indexOf("@");
            var idot=str.indexOf(".");
            var idot2=str.indexOf(".",iat);
            if(iat<1||(iat>idot2)|| idot2>=len-1){
                if(idBaliseCheckEmail != null && idBaliseCheckEmail != undefined){
                    document.getElementById(idBaliseCheckEmail).innerHTML="Email invalide";
                    document.getElementById(idBaliseCheckEmail).style.color="red" ;
                    document.getElementById(idBaliseCheckEmail).focus();
                }
                    return false;
            }else{
                if(idBaliseCheckEmail != null && idBaliseCheckEmail != undefined){
                    document.getElementById(idBaliseCheckEmail).innerHTML="Email ok";
                    document.getElementById(idBaliseCheckEmail).style.color="green";
                }
                    return true;
            }
        }
        if(idBaliseCheckEmail != null && idBaliseCheckEmail != undefined){
            document.getElementById(idBaliseCheckEmail).innerHTML="Email invalide";
            document.getElementById(idBaliseCheckEmail).style.color="red" ;
            document.getElementById(idBaliseCheckEmail).style.visibility = 'visible';
        }
        return false;
}

/*
 * Fonction qui permet de verifier si le texte entré est une année de forme yyyy.
 */
function isElement4DigitsYear(id){
	var numcheck = /\D/;
        var el   = document.getElementById(id);
        var char = el.value;
	for(i=0;i<char.length;i++){
		if(numcheck.test(char.charAt(i))){
                        el.className = 'input_design_incorrect';
			return false;
                    }
	}
        var year = parseInt(char);
        
        if(year>1970 && year<2100){
            el.className = 'input_design';
            return true;
        }
        el.className = 'input_design_incorrect';
	return false;
}

function checkNumberInputed(id){
    var value = document.getElementById(id).value ;
    //Au cas ou l'utilisateur supprime tout dans <input>
    if(value == ''){ 
        document.getElementById(id).value = 0;
        value = 0;
    }
    if(!isNumberDecimal(value)){
        showDialog(' Invalid! ');
        document.getElementById(id).value = 0;
    }
}
function checkIntegerInputed(id){
    var value = document.getElementById(id).value ;
    //Au cas ou l'utilisateur supprime tout dans <input>
    if(value == ''){ 
        document.getElementById(id).value = 0;
        value = 0;
    }
    if(!isNumber(value)){
        showDialog(' Invalid! ');
        document.getElementById(id).value = 0;
    }
}
/*
 * Fonction qui permet de controler le format de date utiliser sur un autre navigateur que Chrome.
 * Elle remplie automatiquement les "/".
 */
function remplireDate(id)
{
    if(document.getElementById(id).type=="text")
    {
        var dateDeb = document.getElementById(id).value;
        var isValidDateEntry = true;
        var strArray = dateDeb.split("/");
        //Verifier que la valeur entré est un nombre
        for(var i = 0; i<strArray.length;i++){
                var str = strArray[i];
                var isNum = isNumber(str);
                
                if(!isNum){
                    isValidDateEntry = false;
                break;
            }
        }
        if(!isValidDateEntry){
            document.getElementById(id).value = "";
            showDialog(' Invalid! ');
        }
        else{
            document.getElementById(id).setAttribute('maxLength',10);
            if(dateDeb.length==2 || dateDeb.length==5)
            {
                document.getElementById(id).value +="/";
            }
        }
    }
}
function isValidDate(id){
    var isValid = false;
    if(document.getElementById(id).type=="text")
    {
        var dateStr = document.getElementById(id).value;
        if(dateStr != undefined && dateStr.split("/").length==3){
            var strArray = dateStr.split("/");
            if(parseInt(strArray[0])>0&&parseInt(strArray[0])<=31
                &&parseInt(strArray[1])>0&&parseInt(strArray[1])<13
                &&parseInt(strArray[2])>1910&&parseInt(strArray[2])<2100){
                document.getElementById(id).className = 'input_design';
                return true;
            }
        }

            showDialog(' Date invalide! ');
        document.getElementById(id).className = 'input_design_incorrect';
    }
    return isValid;
}

function isAnneeFabLessOrEqualDateCircul(idAnneeFab,idDateCircul){
    var anneeFab = document.getElementById(idAnneeFab).value;
    var dateCircul = document.getElementById(idDateCircul).value;
    if(parseInt(anneeFab)<= parseInt(dateCircul.split("/")[2]))
        return true;
    temp_error_msg += "<br>Incoh&eacuterence entre l'ann&eacutee de fabrication et la date de mise en circulation ";
    showDialog(" Incoh&eacuterence entre l'ann&eacutee de fabrication et la date de mise en circulation ");
    return false;
}

function majuscule(id){
    var input = document.getElementById(id);
    input.value = input.value.toUpperCase();
}

function isNumberValid(id){
var value = document.getElementById(id).value ;
    //Au cas ou l'utilisateur supprime tout dans <input>
    if(value == ''){ 
        document.getElementById(id).value = 0;
        value = 0;
    }
    
    //Au cas ou l'utilisateur entre autre chose qu'un chiffre
    if(!isNumberDecimal(value)){
        showDialog(' Invalid! ');
        document.getElementById(id).value = 0;
    }
}

function isATextInteger(id)
{
    var el = document.getElementById(id);
    var text = el.value;
    if(text.length<1){
        el.className = 'input_design_incorrect';
                return false;
    }
            if(!isNumber(text))
            {
                el.className = 'input_design_incorrect';
                return false;
            } 
    
    el.className = 'input_design';
    return true;
}

function getValueOrNull(value){
    if(value == undefined)
        return null;
    if(value == null)
        return null;
    if(value.trim() == '')
        return null;
    return value.trim();
}

function getValueOrNullWith(id){
    if(id != null){
        var el = document.getElementById(id);
        return el != undefined && el != null? getValueOrNull(el.value): null;
    }
    return null;
}

function getSelectValueOrNullWith(id){
    if(id != null){
        var el = document.getElementById(id);
        return el != undefined && el != null && el.value !== '-1'? getValueOrNull(el.value): null;
    }
    return null;
}

function getSelectValueInnerTextOrNullWith(id){
    if(id == null)
        return null;
    var el = document.getElementById(id);
    
    if(el == undefined || el == null)
        return null;
    
    if(el.tagName ==! 'SELECT')
        return null;
    
    if(el.selectedIndex == undefined)
        return null;
    
    if(el.options == undefined)
        return null;
    if(el.options[el.selectedIndex] == undefined)
        return null;
    return el.options[el.selectedIndex].text;
}

function resetFieldValue(){
    for (var i = 0; i < arguments.length; i++) {
        document.getElementById(arguments[i]).value = '';
    }
}

function resetInnerText(){
    for (var i = 0; i < arguments.length; i++) {
        document.getElementById(arguments[i]).innerText = '';
    }
}

function resetInnerHtml(){
    for (var i = 0; i < arguments.length; i++) {
        document.getElementById(arguments[i]).innerHTML = '';
    }
}

function isElementValidEmail(id){
    isElementValidEmailOrEmpty(id, false);
}
function isElementValidEmailOrEmpty(id, isCheckEmpty){
    var el = document.getElementById(id);
    var email = el.value;
    
    if(isCheckEmpty && email != undefined && email != null && email.trim().length == 0){
        return true;
    }
    
    var pattern = /^\w.*?@.*?\..+?/; ///^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    if(email.match(pattern)){
        el.className = 'input_design';
        return true;
    }
    el.className = 'input_design_incorrect';
    return false;
}

function getValueOrEmptyWith(id){
    if(id != null){
        var el = document.getElementById(id);
        return el != undefined && el != null? getValueOrEmpty(el.value): null;
    }
    return '';
}

function getValueOrEmpty(value){
    if(value == undefined)
        return '';
    else if(value == null)
        return '';
    else if(value.trim() == '')
        return '';
    else if(value.trim() == '-1')
        return '';
    return value;
}

function getSelectTextOrEmptyWith(id){
    if(id != null  || undefined){
        var el = document.getElementById(id);
        if(el == undefined|| el==null || el.value == '-1')
            return '';
        
        return el.options[el.selectedIndex].text;
    }
    return '';
}

var max_height = 1280;
var max_width  = 1280;

function resizedImageIfLargerThanMax(imageBase64Str){
    if(imageBase64Str == undefined || imageBase64Str == null)
        return imageBase64Str;
    
    var imageObjt = new Image();
    imageObjt.src = imageBase64Str;
    imageObjt.onload = function() {
        if(image.height>max_height || image.width>max_width){
            imageBase64 = resizeImage(imageObjt);
        }
    };
}

function resizeImage(img){
    var canvas = document.createElement('canvas');
    var width = img.width;
    var height = img.height;

    // calculate the width and height, constraining the proportions
    if (width > height) {
        if (width > max_width) {
            height = Math.round(height *= max_width / width);
            width = max_width;
        }
    } else {
        if (height > max_height) {
            width = Math.round(width *= max_height / height);
            height = max_height;
        }
    }
  
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);
    
    return canvas.toDataURL("image/jpeg",0.7);
}