
function loaderIconVisibility(visibility){
    var el = document.getElementById('loader_icon');
    if(el != undefined && el != null)
        el.style.display=visibility;
}

function showDialog(msg){
    var dialog = document.getElementById('dialog_div');
    var dialog_msg = document.getElementById('dialog_div_msg');
    dialog.style.display = 'block';
    if(msg.length < 55){
        
        dialog_msg.style.textAlign = "center";
    }
    else
        dialog_msg.style.textAlign = "left";
    dialog_msg.innerHTML = msg;
}
function showDialogPage(innerHtml){
    var dialog = document.getElementById('dialogPage_div');
    var dialogContent = document.getElementById('dialogPage_div_content');
    dialog.style.display = 'block';
    if(innerHtml != null && innerHtml != undefined)
        dialogContent.innerHTML = innerHtml;
}
function discardDialogPage(){
    var dialog = document.getElementById('dialogPage_div');
    var dialogContent = document.getElementById('dialogPage_div_content');
    dialog.style.display = 'none'; 
    dialogContent.innerHTML = '';
}
function discardDialog(){
    var dialog = document.getElementById('dialog_div');
    dialog.style.display = 'none';
}

function updatePassword(){
    
    var html = '<div class="textCenter">';
    html +'<div class="textCenter table_title"><span>Modification de mot de passe</span></div>';
    html += '<div class="table_form_block" style="margin: 4px 0;padding: 2px 20px">';
    
    html += '<div>';
    html += '<span>Mot de passe actuel</span>';
    html += '<input id="actualpassword" type="password" class="input_design">';
    html += '</div> <br>';
    
    html += '<div>';
    html += '<span>Nouveau mot de  passe</span>';
    html += '<input id="newpassword" type="password" class="input_design">';
    html += '</div> <br>';
    
    html += '<div>';
    html += '<span>Confirmation nouveau mmot de passe</span>';
    html += '<input id="confirmpassword" type="password" class="input_design">';
    html += '</div> <br>';
    
    html += '</div>';
    html += '</div>';
    
    showYNDialog(html);
    
    document.getElementById('dialogYNMsg_yesBtn').onclick = function(){
        
        
        
        if(isElementTextNotEmpty('actualpassword') 
                && isElementTextNotEmpty('newpassword') 
                && isElementTextNotEmpty('confirmpassword')){
            
            var actualPwd = document.getElementById('actualpassword').value;
            var newPwd = document.getElementById('newpassword').value;
            var confirmPwd = document.getElementById('confirmpassword').value;
            
            if(newPwd != confirmPwd){
                showDialog('Le nouveau mot de passe et la confirmation ne correspondent pas!');
            }
            else{
            var params = {
                
                "pass0" : actualPwd,
                "pass1" : newPwd,
                "pass2" : confirmPwd
                
            };
            
            toExecute1 = function(xmlHttp){
                var jobj = JSON.parse(xmlHttp.responseText);
                if(jobj.error.errorCode != '200'){
                    showDialog(jobj.error.errorDescription);
                }
                else{
                    showDialog('Succ&egrave;!');
                    discardYNDialog();
                    
                    setTimeout(function(){
                        document.getElementById('loggoutButton').click();
                    }, 1500);
                    
                }
                toExecute1 = undefined;
            };
            
            executeAction1('flashupdatepassword' , JSON.stringify(params), contentTypeJSON );
            }
        }
        
    };
}

function showYNDialog(msg){
    var dialog = document.getElementById('dialogYNMsg_div');
    var dialog_msg = document.getElementById('dialogYNMsg_div_msg');
    dialog.style.display = 'block';
    
    dialog_msg.innerHTML = msg;
}

function discardYNDialog(){
    var dialog = document.getElementById('dialogYNMsg_div');
    dialog.style.display = 'none'; 
    document.getElementById('dialogYNMsg_yesBtn').onclick = function(){
        discardYNDialog();
        };
}

function isElementTextNotEmpty(id){
    
    var el = document.getElementById(id);
    var str = el.value;
    if(str !== undefined && str.trim().length > 0 && str.trim() != ''){
        el.className = 'input_design';
        return true;
    }
    el.className = 'input_design_incorrect';
    return false;
}

