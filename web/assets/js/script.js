var styleReset = {
    borderBottom: "1px solid #d0d0d0",
    backgroundColor: "#f8f8f8"
};
var styleConfig = {
    borderBottom: "15px solid #ffde08",
    backgroundColor: "white"
};
var flash, mobile;

function showMethod(method){
    $('.method').hide();
    $('#'+ method).fadeIn();
}

$('#card').on('click', function(e){
    $(".menu").css(styleReset);
    $(this).css(styleConfig);

    showMethod('methodcard');
});

$('#cash').on('click', function(e){
    $(".menu").css(styleReset);
    $(this).css(styleConfig);

    showMethod('methodcash');
});


$("#flash").click(function () {
    $(".menu").css(styleReset);
    $(this).css(styleConfig);

    showMethod('methodflash');

    $("#polygonflash").hover(function () {
        $(this).css("fill", "#ffff01");
    },
            function () {
                $(this).css("fill", "#ffde08");
            })


});
$("#pay").click(function () {
    $(".menu").css(styleReset);
    $(this).css(styleConfig);


    showMethod('methodcash');

    $("#polygonflash").hover(function () {
        $(this).css("fill", "#ffff01");
    },
            function () {
                $(this).css("fill", "#ffde08");
            })


});

$("#mobile").click(function () {
    $(".menu").css(styleReset);
    $(this).css(styleConfig);

    showMethod('methodmobile');

    $("#polygonmobile").hover(function () {
        $(this).css("fill", "#ffff01");
    },
    function () {
        $(this).css("fill", "#ffde08");
    })



});

$("#polygoncard").hover(function () {
    $(this).css("fill", "#ffff01");
},
        function () {
            $(this).css("fill", "#ffde08");
        })


document.addEventListener("DOMContentLoaded", function (event) {


    $("#card").css(styleConfig);
    $("flash").css(styleReset);
    $(".mobile").css(styleReset);

    showMethod('methodcard');

    $("#polygoncard").hover(function () {
        $(this).css("fill", "#ffff01");
    },
    function () {
        $(this).css("fill", "#ffde08");
    })



});
