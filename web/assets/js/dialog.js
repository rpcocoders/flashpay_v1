
function closeDialog(id){
    document.getElementById(id).style.display = "none";
}

function showDialogBlock(id,pcn, prenom, nom, postnom){
    document.getElementById(id).style.display = "block";
    document.getElementById('nomBenPcnSpan').innerHTML = pcn;
    document.getElementById('prenomModif').value = prenom;
    document.getElementById('nomModif').value = nom;
    document.getElementById('postnomModif').value = postnom;  
}

function showDialogBlock1(id,pcn, noms){
    document.getElementById(id).style.display = "block";
    document.getElementById('nomBenPcnSpan').innerHTML = pcn;
    document.getElementById('nomBen0Span').value = noms;
    document.getElementById('nomModif').value = '';
    document.getElementById('postnomModif').value = '';  
}