/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const page = $('#content');
const loadingDiv = $('#loadingDiv');
const loadingDivMessage = $('#messageLoadingDiv');

$(document).ready(function(){
    loadingDiv.hide();

    // $('#charger').on('click', function(event){
    //     openPageByUrl('/utils/page');
    // });
    $('#hide').on('click', function(event){
        hideLoadingDiv();
    });
    
    
});

const IP = 'localhost';
const SCHEMAS = 'http';
const PORT = '8089';
const BASE = SCHEMAS + '://' + IP + ':' + PORT + '/flashpay/';
const URL_INIT_FLASHAPP_PAIEMENT = BASE + 'init-flashapp-paiement';
const URL_INIT_MOBILEMONEY_PAIEMENT = BASE + 'init-mobilemoney-paiement'
const URL_SESSION_EXPIRED = BASE + 'session-expired';
const URL_CONFIRME_PAIEMENT_FLASHAPP = BASE + "confirm-paiement-flashpp";

const TYPE_FLASH = "FLASH";
const TYPE_MOBILE_MONEY = "MOBILE MONEY";
const TYPE_VISA_CARD = "VISA";

const URL_DECLINE = $('#url-decline').val();
const URL_SUCCESS = $('#url-success').val();

function showLoading(msg){
    let message = ' Veuillez patienter';

    if(typeof msg !== 'undefined')
        message = msg;
    
    loadingDivMessage.text(message);
    loadingDiv.fadeIn();
}

function hideLoadingDiv(){
    loadingDiv.hide();
}

function openPageByUrl(url){
    showLoading();

    $.get({
        url: url
    }, function(response){
        hideLoadingDiv();
        console.log(response)
        page.html(response);
    }).fail(function(error){
        hideLoadingDiv();
        // do something if error
    })
}

function openPage(object, params){
    showLoading();

    let url = $(object).attr("page");
    console.log(object)

    $.get({
        url: url
    }, function(response){
        hideLoadingDiv();
        page.html(response);
    }).fail(function(error){
        hideLoadingDiv();
        // do something if error
    })        
}

function sessionExpired(){
    document.location = URL_SESSION_EXPIRED;
}

function httpRequest(urlApi, data){
    return new Promise((resolve, reject) => {
        $.post(
            {
                url: urlApi,
                data: data
            },
            function(response){
                console.log(response);
                if(response.error.code != 'OK'){
                    if(response.error.description == 'Votre session a expiré.')
                        sessionExpired();
                    reject(response.error.description);
                }
                resolve(response);
            }
        ).fail(function(eror){
            reject("Une erreur s'est produite. Veuillez reessayer ou contacter le support technique");
        })
    })
}

$('#confirmSms').hide();

$('#btn-valider-fpi').on('click', async function(e){
    let msg = $('#text-fpi-message');
    let fip_field = $('#fpi_field');
    
    if(fip_field.val().length < 8){
        msg.addClass('text-danger');
        msg.text('Flash ID incorrect.');
        return;
    }

    let data = {};

    data.fpi = fip_field.val();

    httpRequest(URL_INIT_FLASHAPP_PAIEMENT, JSON.stringify(data))
    .then(
        res =>{
            $('#boxFip').hide();
            $('#confirmSms').fadeIn();

        }
    )
    .catch(error => {
        msg.addClass('text-danger');
        msg.html(error);
    });
    
});

$('#fpi_field').on('keyup', function(e){
    let msg = $('#text-fpi-message');
    
    if($('#fpi_field').val().length > 0){
        msg.removeClass('text-danger');
        msg.text("Vous allez recevoir un code d'activation par sms");
        return;
    }
    
});

$('#codeValidation').on('keyup', function(e){
    let msg = $('#text-codeValidation-message');
    
    if($('#codeValidation').val().length > 0){
        msg.removeClass('text-danger');
        msg.text("");
        return;
    }
    
});

$('#send-code').on('click', async function(e){
    let msg = $('#text-codeValidation-message');
    let fip_field = $('#codeValidation');
    
    if(fip_field.val().length < 6){
        msg.addClass('text-danger');
        msg.text('code incorrect.');
        return;
    }

    let data = {};

    $('#send-code').hide();

    data.code = fip_field.val();

    msg.html(
        '<span><i class="fa fa-spinner fa-pulse"></i> Opération en cours...</span>'
    );

    httpRequest(URL_CONFIRME_PAIEMENT_FLASHAPP, JSON.stringify(data))
    .then(
        res =>{
            window.location = URL_SUCCESS;
        }
    )
    .catch(error => {
        
        msg.html('<span>'+error+'</span>');
        setTimeout(() =>{
            window.location = URL_DECLINE;
        }, 6000);
    });
    
});

$('#phoneNumber').on('keyup', function(e){
    let msg = $('#text-mobilemoney-message');
    
    if($('#phoneNumber').val().length > 0){
        msg.removeClass('text-danger');
        msg.text("");
        return;
    }
    
});

$('#sendPhone').on('click', function(e){
    let msg = $('#text-mobilemoney-message');
    let fip_field = $('#phoneNumber');
    
    if(fip_field.val().length < 10){
        msg.addClass('text-danger');
        msg.text('Numéro de téléphone incorrect.');
        return;
    }

    let data = {};

    //$('#sendPhone').disable(true);

    data.telephone = fip_field.val();

    msg.html(
        '<span><i class="fa fa-spinner fa-pulse"></i> Opération en cours...</span>'
    );

    httpRequest(URL_INIT_MOBILEMONEY_PAIEMENT, JSON.stringify(data))
    .then(
        res =>{
            $('#phone-box').hide();
            $('.message-box').fadeIn();
            //window.location = URL_SUCCESS;
        }
    )
    .catch(error => {
        
        msg.html('<span>'+error+'</span>');
        // setTimeout(() =>{
        //     window.location = URL_DECLINE;
        // }, 6000);
    });
});








