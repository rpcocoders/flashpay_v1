var xmlHttp;
var internalErrorMsg = 'Une erreure interne vient de se produire. veuillez re&eacutessayer';
var resourceNotFoundMsg = 'Resource introuvable. Code: 404';
var dropDownElementsToSet; /* 2d Array : contient une liste de menu deroulant à remplir et valeur à envoyer par Ajax*/
var contentTypeJSON = "application/json;charset=UTF-8";
var contentTypeUrlEncoded = "application/x-www-form-urlencoded";
var successStr = 'succ&egraves';
var commonInterfaceUrl = "/navigation?p=url32";
var not_found_txt = 'Non retrouv&eacute!';
var enterDataCorrectly = 'Entrez corr&egrave;ctement les donn&eacute;es requises!';
var callBack;


function getXmlHttpObject(){
	var xmlHttp;
	try{  // Firefox, Opera 8.0+, Safari  
		xmlHttp=new XMLHttpRequest();  }
	catch (e) {  // Internet Explorer
		try{xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");}
		catch (e){
   			try{ xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}
   			catch (e){
   				alert("Your browser does not support AJAX!");
   			}
   		}
   	}
   	return xmlHttp;
}

function executeAction(loc, params){
    executeAction2(loc, params, null,contentTypeUrlEncoded);
}
function executeAction(loc, params, action){
    executeAction2(loc, params, action,contentTypeUrlEncoded);
}
function executeAction1(loc, params, contenttype){
    executeAction2(loc, params, null,contenttype);
}
function executeAction2(loc, params, action, contenttype){
    //Requetes pour des actions au serveur
  var xmlHttp = getXmlHttpObject();
	xmlHttp.open("POST",loc,true);
        xmlHttp.setRequestHeader("Content-type", contenttype);
        loadingIconDivVisibility('inline-block');
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState===4){
                    loadingIconDivVisibility('none');
                    var httpResponseCode = xmlHttp.getResponseHeader('resultCode');
                    if(httpResponseCode == '0'){
                        //Reconnexion
                        window.location = "/home.jsp";
                    }
                    else if(xmlHttp.status == 500){
                        showDialog(internalErrorMsg);
                    }
                    else if(xmlHttp.status == 404){
                        showDialog(resourceNotFoundMsg);
 
                    }
                    else if(httpResponseCode == '200' || httpResponseCode == '201'){
                                showDialog(successStr+'!');
                                
                        }
                        else if(httpResponseCode == '404'){
                                showDialog(resourceNotFoundMsg);
                        }
                        else{
                             console.log('LOG  22');
                            if(callBack != undefined){  callBack(xmlHttp);  }
                            else{
                                var errorMsg = xmlHttp.getResponseHeader("errorMsg");
                                if(errorMsg != null && errorMsg != undefined){
                                    showDialog(getEncodedText(errorMsg) );
                                }
                                else
                                    showDialog('Echec!');
                            }
                        }
		}
            };
	xmlHttp.send(params);
}

function showmessage(loc ,destinationid){
    showmessage1(loc ,destinationid, null);	
}
function showmessage1(loc ,destinationid,params){
    showmessage2(loc ,destinationid,params,"application/x-www-form-urlencoded");
}
function showmessage2(loc ,destinationid,params, contentType){
    xmlHttp = getXmlHttpObject();
	xmlHttp.open("POST",loc,true);
        xmlHttp.setRequestHeader("Content-type", contentType);
        loadingIconDivVisibility('inline-block');
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState===4){
                    loadingIconDivVisibility('none');
                    var resultCode = xmlHttp.getResponseHeader('resultCode');
                    
                    try{
                        
                        tokenEnvoiTransaction = undefined;
                        
                    }
                    catch(err){
                        console.log(err);
                    }
                    
                    if(resultCode == '0'){
                        //Reconnexion
                        window.location = commonInterfaceUrl;
                    }
                    else if(resultCode == '400' || resultCode == '500'){
                            var errorMsg = xmlHttp.getResponseHeader('errorMsg');
                            if(errorMsg != null){
                                errorMsg = getEncodedText(errorMsg);
                                showDialog(errorMsg);
                            }
                        }
                    else if(xmlHttp.status == 500){
                        showDialog(internalErrorMsg);
                    }
                    else if(xmlHttp.status == 404){
                        showDialog(resourceNotFoundMsg);
                    }
                    else{
                        document.getElementById(destinationid).innerHTML=xmlHttp.responseText.trim();
                        //eval(xmlHttp.responseText);
                        var codes = document.getElementById(destinationid).getElementsByTagName("script");
                        
                        for(var i=0;i<codes.length;i++) {
                            eval(codes[i].text);
                        } 
                        
                        if(callBack != undefined) callBack(xmlHttp);
                    }
		}
	};
	xmlHttp.send(params);
}
function getEncodedText(errorMsg){
    var msg = decodeURIComponent( errorMsg);
                        String.prototype.replaceAll = function(target, replacement) {
                           return this.split(target).join(replacement);
                        };
                        msg = msg.replaceAll("+", " ");
                        
                        return msg;
}

function showdata(loc,dest,data,table)
{
     xmlHttp = getXmlHttpObject();
	xmlHttp.open("POST",loc+"?data="+data+"&table="+table,true);
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState===4){
                    
                    document.getElementById(dest).innerHTML=xmlHttp.responseText.trim();
                      
		}
	};
	xmlHttp.send(data);
}
    
    //-------------------- Dropdown Lists -----------------------
/*Le dernier parametre  , permet d'appeller la methode pour une execution succedant l'actuelle*/
function setDropDownElement(elementId, value, elementToReplace, elementToReplaceDefaultValue, nextCallElement){

    var xmlHttp = getXmlHttpObject();
    loadingIconDivVisibility('inline-block');
    var params = "elementId="+elementId+"&value="+encodeURIComponent(value)+"&p=url22";
    xmlHttp.open("POST","navigation",true);
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState===4){
                    var httpResponseCode = xmlHttp.getResponseHeader('resultCode');
                    loadingIconDivVisibility('none');
                    if(httpResponseCode == '0'){
                        //Reconnexion
                        window.location = commonInterfaceUrl;
                    }
                    else if(xmlHttp.status == 500){
                        showDialog(internalErrorMsg);
                    }
                    else if(xmlHttp.status == 404){
                        showDialog(resourceNotFoundMsg);
                    }
                    else  if(httpResponseCode== 201  || httpResponseCode== 200
                            || (xmlHttp.responseText != undefined && xmlHttp.responseText.includes('<select'))){
                        if( elementToReplace == 'originDestination'){
                            setOriginDestination(xmlHttp.responseText);
                        }
                        else{
                            var el = document.getElementById(elementToReplace);
                            if(el == null && elementId == 'region'){
                                el = document.getElementById(elementToReplace+'2');
                            }
                            el.innerHTML = xmlHttp.responseText;
                            if(elementToReplaceDefaultValue != null){
                                el.value = elementToReplaceDefaultValue;
                            }
                            if(nextCallElement != null)
                            {
                                setDropDownElement(nextCallElement[0],nextCallElement[1],nextCallElement[2],nextCallElement[3]);
                            }
                            else if(elementId == "region"){
                                checkIfOnlyOneTown(el);
                            }
                        }
                    }
                    else{

                        showDialog('Veuillez réessayer');
                    }
		}
            };
	xmlHttp.send(params);
}

function addDropDownMenuToSet(elementId, value, elementToReplace, elementToReplaceDefaultValue){
    
    if(dropDownElementsToSet == undefined){
        dropDownElementsToSet = new Array();
    }
    var values = [elementId, value, elementToReplace, elementToReplaceDefaultValue];
    dropDownElementsToSet.push(values);
    
}
//Remplir Tous les menus deroulants de la page, 1 par 1, dont la liste d'info est dans la 2d array dropDownElementsToSet
function setPageDropDownElements(){
    
    if(dropDownElementsToSet != undefined){
    loadingIconDivVisibility('inline-block');    
    var values = dropDownElementsToSet.pop();
    var elementId = values[0];
    var value     = values[1];
    var elementToReplace= values[2];
    var elementToReplaceDefaultValue = values[3];
    var params = "elementId="+elementId+"&value="+value;
    xmlHttp.open("POST","asynchronousLists.jsp",true);
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState===4){
                    var httpResponseCode = xmlHttp.getResponseHeader('resultCode');
                    loadingIconDivVisibility('none');
                    if(httpResponseCode == '0'){
                        //Reconnexion
                        window.location = commonInterfaceUrl;
                    }
                    else if(xmlHttp.status == 500){
                        showDialog(internalErrorMsg);
                    }
                    else if(xmlHttp.status == 404){
                        showDialog(resourceNotFoundMsg);
                    }
                    else if(httpResponseCode == '201'  || httpResponseCode == '200' 
                            || (xmlHttp.responseText != undefined && xmlHttp.responseText.includes('<select'))){
                            
                            var el = document.getElementById(elementToReplace);
                            el.innerHTML = xmlHttp.responseText;
                            if(elementToReplaceDefaultValue != null){
                                el.value = elementToReplaceDefaultValue+'';
                            }
                            if(dropDownElementsToSet.length > 0)
                            {
                                setPageDropDownElements();
                            }
                        }
                    else{
                            showDialog('Veuillez reéssayer');
                        }
		}
            };
	xmlHttp.send(params);
    }
}

function getEncodedText(errorMsg){
    var msg = decodeURIComponent( errorMsg);
                        String.prototype.replaceAll = function(target, replacement) {
                           return this.split(target).join(replacement);
                        };
                        msg = msg.replaceAll("+", " ");
                        
                        return msg;
}

function loadingIconDivVisibility(visibility){
    var el = document.getElementById('loader_icon');
    if(el != undefined)
        el.style.display= visibility;
}

function showDialog(msg){
    var dialog = document.getElementById('dialog_div');
    var dialog_msg = document.getElementById('dialog_div_msg');
    dialog.style.display = 'block';
    if(msg.length < 55){
        
        dialog_msg.style.textAlign = "center";
    }
    else
        dialog_msg.style.textAlign = "left";
    dialog_msg.innerHTML = msg;
}