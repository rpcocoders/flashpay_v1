var navigationURL = 'navigation';
var areYouSureStr = '&Ecirctes-vous s&ucircr';
function loadingIconDivVisibility(visibility){
    var el = document.getElementById('loader_icon');
    if(el != undefined)
        el.style.display= visibility;
}

function discardDialog(){
    var dialog = document.getElementById('dialogMsg_div');
    var dialog1 = document.getElementById('dialog_view_div');
    dialog.style.display = 'none';
    if(dialog1 != undefined)
        dialog1.style.display = 'none';
    
}

function discardDialog1(){
    var dialog = document.getElementById('dialogMsg1_div');
    dialog.style.display = 'none';
}

function showDialog(msg){
    var dialog = document.getElementById('dialogMsg_div');
    var dialog_msg = document.getElementById('dialogMsg_div_msg');
    dialog.style.display = 'block';
    if(msg.length < 53)
        dialog_msg.style.textAlign = "center";
    else{
        dialog_msg.style.textAlign = "left";
        dialog_msg.style.fontSize = "0.9em";
    }
    dialog_msg.innerHTML = msg;
}

function showDialog1(msg){
    var dialog = document.getElementById('dialogMsg1_div');
    var dialog_msg = document.getElementById('dialogMsg1_div_msg');
    dialog.style.display = 'block';
    if(msg.length < 53)
        dialog_msg.style.textAlign = "center";
    else
        dialog_msg.style.textAlign = "left";
    dialog_msg.innerHTML = msg;
}

function handleNavigation(page){

            switch (page) {
                case "":
                case "#":
                        break;
                    
                case "#send":
                    showmessage("sending.jsp","contenu");
                    break;
                    
                case "#payout":
                    showmessage("receive.jsp","contenu");
                    break;
                case "#home":
                    showmessage("home.jsp","contenu");
                    break;
                case "#territoryinformation":
                    showmessage("territoryinformation.jsp","contenu");
                    break;
                    case "#cashbalancereport":
                    showmessage("cashbalancereport.jsp","contenu");
                    break;
                case "#changepassword":
                    showmessage("changepassword.jsp","contenu");
                    break;
                case "#activityreport":
                    showmessage("activityreport.jsp","contenu");
                    break;
                case "#searchtransferstatus":
                    showmessage("searchtransferstatus.jsp","contenu");
                    break;
                case "#quickpay":
                    showmessage("quickpay.jsp","contenu");
                    break;
            }
                
}
function goToPage(page){
    window.location = page;
}

function discardReportView(){
    var dialog = document.getElementById('dialog_view_div');
    dialog.style.display = "none";
}
function numImpotDisplay(displayStr){
    var text = 'Numéro ID';
    var nomText = 'Nom';
    if(displayStr == 'none'){
        text = 'N° Impôt';
        setTypeID('IDENTIFICATION NATIONALE');
        nomText = "Nom de l'entreprise";
        nom_txt = "NOM DE L'ENTREPRISE";
    }
    else{
        resetSelectElement('idType');
        nom_txt = "NOM";
    }
    document.getElementById('idNumSpan').innerHTML = text;
    document.getElementById('nomSpan').innerHTML = nomText;
    document.getElementById('postnom_block').style.display = displayStr;
    document.getElementById('prenom_block').style.display = displayStr;
    document.getElementById('sexe_block').style.display = displayStr;
    //document.getElementById('permisConduire_block').style.display = displayStr;
    
    document.getElementById('nom').value ="";
    document.getElementById('postnom').value ="";
    document.getElementById('prenom').value ="";
    //document.getElementById('permisConduire').value ="";
}
function setTypeID(text){
    var element = document.getElementById('idType');
    var index = 0;
    var options = element.options;
    for(;index<options.length;index++){
        if(text == options[index].text)
            break;
    }
    element.selectedIndex = index;
}
function resetSelectElement(id){
    document.getElementById(id).selectedIndex=0;
}
function showData(idvehicule){
    showOverlayBlock();
    var params = 'idvehicule='+idvehicule;
    showmessage1('dataView.jsp','dialogContent', params);
}

function showReprintConfirm(idvehicule){
    showOverlayBlock();
    var params = 'idvehicule='+idvehicule+'&obj=confirm';
    showmessage1('dataView.jsp','dialogContent', params);
}

function showOverlayBlock(){
     
    var dialog = document.getElementById('dialog_view_div');
    var dialogContent = document.getElementById('dialogContent');
    dialogContent.innerHTML = "";
    /*------ loading image block -----------*/
    //var imgDiv = createLoadingImageBlock();
    //dialogContent.appendChild(imgDiv);
    /*--------------------------------------*/
    dialog.style.display = "block";
}
function createLoadingImageBlock(){
    var imgDiv = document.createElement("div");
    var img = document.createElement("img");
    imgDiv.id = "loaderImgDiv";
    imgDiv.className = "loaderGif_block";
    img.className = "loaderGif";
    img.src = "images/loader.gif";
    imgDiv.appendChild(img);
    
    return imgDiv;
}

function onSelectChange(elementId,value){
    // Si value = -1 , veut dire que c'est la phrase 'Sélectionnez...' qui a été sélectionné
    if(value != '-1'){
        switch(elementId){
            case "adresseProvince": 
                setDropDownElement("adresseProvince", value, 'adresseVille',null,null);
                break;
              
             case "vehiculeMarque": 
                setDropDownElement("vehiculeMarque", value, 'vehiculeModele',null,null);
                break;
              
            case "devise":
                //executeAction('asynchronousDataRetrieval.jsp','obj='+montant_txt+'&iddevise='+value,montant_txt);
                changeDeviseMontant(value);
                break;
                
                case "filtrePays": 
            setDropDownElement("pays", value, 'filtreRegion',null,null);
            break;
                
            case "filtreRegion":
            setDropDownElement("region", value, 'filtreVille', null,null);
            break;
            
        case "filtreVille":
                    resetDropdown('filtreTypeAgence');
                    setDropDownElement("ville", value, 'agences', null,null);
            break;
            
        case "filtreVille2":
                    resetDropdown('filtreTypeAgence');
                    setDropDownElement("ville", value, 'originDestination', null,null);
            break;
            
        case "filtreTypeAgence":
            value += ','+document.getElementById("filtreVille").value; 
            setDropDownElement("typeAgence", value, 'filtreAgence', null,null);
            break;
            /*
            case "vehiculeType":
            executeAction('asynchronousDataRetrieval.jsp','obj='+montant_txt+'&idtypevehicule='+value,montant_txt);
            break;
            
            case "typeTransactionInv":
                setDeviseToDisplay(value);
                break;
                */
        }
    }
}

function setOriginDestination(innerHtml){
    document.getElementById('filtreAgenceOrigin').innerHTML = innerHtml;
    document.getElementById('filtreAgenceDestination').innerHTML = innerHtml;
}

//Re-initialiser un menu deroulant en selectionnant son 1er contenu
function resetDropdown(id){
    var dropdown = document.getElementById(id);
    if(dropdown != undefined)
        dropdown.selectedIndex = 0;
}

function typeVehiculeChange(){
    var typeVehiculeSelect  = document.getElementById('vehiculeType');
    var classificationSelect = document.getElementById('vehiculeClass');
    var str;
    if(typeVehiculeSelect.options[typeVehiculeSelect.selectedIndex].text.includes('MOTO')){
        str = 'MOTO';
        var prixMoto = document.getElementById('montantInvMoto').value;
        var taux = document.getElementById('tauxUsdCdf').value;
        var montantCDF = prixMoto * taux;
        
        document.getElementById('montant').value = prixMoto;
        document.getElementById('montantCDF').value = montantCDF;
    }
    else{
        str = 'VOITUR';
        
        var prixVehicule = document.getElementById('montantInv').value;
        var taux = document.getElementById('tauxUsdCdf').value;
        var montantUSD = prixVehicule * taux;
        
        document.getElementById('montant').value = prixVehicule;
        document.getElementById('montantCDF').value = montantUSD;
    }
    var classOptions = classificationSelect.options;
        for(var i=0;i<classOptions.length;i++){
            if(classOptions[i].text.includes(str)){
                classificationSelect.selectedIndex = i;
                break;
            }
        }
}

function switchElementsDisplay(elIdToShow,elIdToHide){
    var el1 = document.getElementById(elIdToShow);
    var el2 = document.getElementById(elIdToHide);
    if(el1 != undefined)
        el1.style.display = 'block';
    if(el2 != undefined)
        el2.style.display = 'none';
}
function discardReportCreationDialog(id){
    document.getElementById(id).style.display = 'none';
        //dialogCommentTextAreaDisplay('none');
}

function confirmBox(msg,id){
    var dialog = document.getElementById(id);
    document.getElementById('recap_env_question_block').innerHTML = msg;
    dialog.style.display = 'block';
}

function navigateTo(params){
    showmessage1(navigationURL,"contenu",params+'&showmessage=1');
}

function setWelcomeFCanim(e){
    var threeddiv = document.getElementById('threeddiv');
    var rect = threeddiv.getBoundingClientRect();

       var actualX = (e.clientX-rect.left);
       var actualY = (e.clientY-rect.top);
       var incrementX = threeddiv.clientWidth/14;
       var incrementY = threeddiv.clientHeight/10;
       var xhalf = threeddiv.clientWidth/2;
       var yhalf = threeddiv.clientHeight/2;
       
       var degX = actualX<xhalf?'-'+Math.abs(incrementX-(actualX/incrementX)):Math.abs(incrementX-(actualX/incrementX))+'';
       var degY = actualY>yhalf?'-'+Math.abs(incrementY-(actualY/incrementY)):Math.abs(incrementY-(actualY/incrementY))+'';
       var els = document.getElementsByClassName('layer');
       for(var i=0;i<els.length;i++){
            els[i].style.transform="rotateY("+degX+"deg) rotateX("+degY+"deg) translateZ(0)";
        }
}

function showDialogPage(innerHtml){
    var dialog = document.getElementById('dialogPage_div');
    var dialogContent = document.getElementById('dialogPage_div_content');
    dialog.style.display = 'block';
    if(innerHtml != null && innerHtml != undefined)
        dialogContent.innerHTML = innerHtml;
}

function discardDialogPage(){
    var dialog = document.getElementById('dialogPage_div');
    var dialogContent = document.getElementById('dialogPage_div_content');
    dialog.style.display = 'none'; 
    dialogContent.innerHTML = '';
}
function authentifyTransaction(obj,isLite){
    
    var login = document.getElementById('login').value;
    var mdp   = document.getElementById('mdp').value;
    if(isElementTextNotEmpty('login')){
        var params = 'obj='+obj+'&login='+encodeURIComponent(login)+'&pass='+encodeURIComponent(mdp);
        showmessage1('asyncauthentication','contenu',params);
    }
}

function showYNDialog(msg){
    var dialog = document.getElementById('dialogYNMsg_div');
    var dialog_msg = document.getElementById('dialogYNMsg_div_msg');
    dialog.style.display = 'block';
    
    if(msg != undefined && msg != null)
        dialog_msg.innerHTML = msg;
}

function discardYNDialog(){
    var dialog = document.getElementById('dialogYNMsg_div');
    dialog.style.display = 'none'; 
    document.getElementById('dialogYNMsg_yesBtn').onclick = function(){
        discardYNDialog();
        };
}

function showYNDialogCancel(msg){
    var dialog = document.getElementById('dialogYNMsg_div_cancel');
    var dialog_msg = document.getElementById('dialogYNMsg_div_msg_cancel');
    dialog.style.display = 'block';
    
    if(msg != undefined && msg != null)
        dialog_msg.innerHTML = msg;
}

function showLoadDialog(){
    var dialog = document.getElementById('dialogLoad');
   // var dialog_msg = document.getElementById('dialogYNMsg_div_msg');
    dialog.style.display = 'block';

}

function discardDialogLoad(){
    var dialog = document.getElementById('dialogLoad');
    dialog.style.display = 'none'; 
}


function showGetCodeYNDialog(msg){
    var dialog = document.getElementById('dialogYNMsg_div');
  //  var dialog_msg = document.getElementById('dialogYNMsg_div_msg');
    dialog.style.display = 'block';
    
  //  dialog_msg.innerHTML = msg;
}