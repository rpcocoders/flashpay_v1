<%-- 
    Document   : main
    Created on : 5 mars 2021, 00:39:32
    Author     : rubenpanga
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="assets/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- <style>
            body{
                background-image: url(${pageContext.request.contextPath}/assets/images/background.png);
            }
        </style> -->
    </head>
    <body>
        <input type="hidden" name="" id="url-success" value="${marchand.urlSuccess}">
        <input type="hidden" name="" id="url-decline" value="${marchand.urlDecline}">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12 col-xl-6">
                    <div class="methodtext" id="green">
                        <p style="text-align: center;">Choissisez Votre Methode de Payement</p>
                    </div>
                    <div class="methodtype row">
                        <div class="typecontainer menu col-3 col-xl-12" id="flash">
                            <div class="type1icon">
                                <img class="option-logo" src="assets/images/flashicon.png" alt="FlashApp" width="100">
                            </div>
                            <div class="type1text"><span>FlashApp(FLASH TO FLASH)</span></div>
                        </div>
                        <div class="typecontainer menu col-3 col-xl-12" id="card">
                            <div class="type1icon">
                                <img class="option-logo" src="assets/images/credit-card.png" alt="credit-card" width="100" height="100">
                            </div>
                            <div class="type1text"><span>Credit & Debit cards</span></div>
                        </div>
                        <div class="typecontainer menu col-3 col-xl-12" id="mobile">
                            <div class="type1icon">
                                <img class="option-logo" src="assets/images/smartphone.png" alt="Mobiel Money" width="100" height="100">
                            </div>
                            <div class="type1text"><span>Mobile Money</span></div>
                        </div>
                        <div class="typecontainer menu col-3 col-xl-12" id="cash">
                            <div class="type1icon">
                                <img class="option-logo" src="assets/images/smartphone.png" alt="Mobiel Money" width="100" height="100">
                            </div>
                            <div class="type1text"><span>FlashPay(Coupon cash)</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-12 col-xl-6">
                    <div id="marchand-box" class="col-12 mt-5 p-3 border shadow-sm rounded">
                        <h4 class="font-weight-bold">PAIEMENT ${marchand.nomMarchand}</h4>
                        <span class="font-weight-bold d-block">Montant: ${marchand.montant} ${marchand.deviseiso}</span>
                        <span class="font-weight-bold">Transaction ID:  ${marchand.transactionId}</span>
                        <div class="font-weight-bold">${marchand.commentaire}</div>
                    </div>
                    <div class="method border rounded p-3 mt-2 shadow-sm rounded"  id="methodflash">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-4 offset-4">
                                    <img src="assets/images/logo.png" alt="ltp" class="d-block w-100">
                                </div>
                            </div>
                         </div>
                        <div class="col-12 p-0" id="boxFip">
                            <div class="form-group">
                                <label for="fpi_field" class="font-weight-bold">FLASH ID</label>
                                <input type="text" maxlength="8" class="form-control form-control-sm" id="fpi_field" aria-describedby="emailHelp">
                                <small id="text-fpi-message" class="form-text font-weight-bold">Vous allez recevoir un code d'activation par sms</small>
                            </div>
                            <svg height="50" width="200" id="btn-valider-fpi" class="m-0 cursor">
                                <polygon points="15 25, 25 40, 150 40, 160 25, 150 10, 25 10" style="fill:#ffde08" id="polygonmobile"/>
                                <text x="65" y="30" fill="black">Payer</text>
                                Sorry, your browser does not support inline SVG.
                            </svg>
                        </div>
                        <div class="col-12 p-0" id="confirmSms">
                            <label class="font-weight-bold">CODE DE VALIDATION</label>
                            <input type="text" maxlength="6" class="form-control form-control-sm" id="codeValidation" placeholder="">
                            <span id="text-codeValidation-message" class="form-text font-weight-bold mt-2"></span>
                            <svg height="50" width="500" class="m-0 cursor" id="send-code">
                            <polygon points="15 25, 25 40, 400 40, 410 25, 400 10, 25 10" style="fill:#ffde08" id="polygoncard" />
                            <text x="180" y="30" fill="black">OK</text>
                            Sorry, your browser does not support inline SVG.
                            </svg>
                        </div>
                    </div>
                    <div class=" method border rounded p-3 mt-2 shadow-sm rounded"  id="methodcard">
                        <div class="col-12 mb-4">
                            <div class="row">
                                <div class="col-6 offset-3">
                                    <img src="assets/images/visa (2).png" alt="ltp" class="d-block w-100">
                                </div>
                            </div>
                         </div>
                        <div class="col-12">
                            <svg height="50" width="500" id="svgcard">
                            <polygon points="15 25, 25 40, 400 40, 410 25, 400 10, 25 10" style="fill:#ffde08" id="polygoncard" onclick="doPayVisa()" />
                            <text x="180" y="30" fill="black">Continuer</text>
                            Sorry, your browser does not support inline SVG.
                            </svg>
                        </div>
                    </div>
                    <div class=" method border rounded p-3 mt-2 shadow-sm rounded"  id="methodmobile">
                        <div class="col-12 mb-3">
                            <div class="row">
                                <div class="col-3">
                                    <img src="assets/images/logompesa.png" alt="ltp" width="100">
                                </div>
                                <div class="col-3">
                                    <img src="assets/images/logoorangemoney1.png" alt="ltp" width="100">
                                </div>
                                <div class="col-3">
                                    <img src="assets/images/AirtelmoneylogoHD.jpg" alt="ltp" width="100">
                                </div>
                                <div class="col-3">
                                    <img src="assets/images//AfriMoney.jpg" alt="afrimoney" width="100">
                                </div>
                            </div>
                         </div>
                        <div id="phone-box">
                            <div class="col-12">
                                <label>Numéro de téléphone</label>
                                <br>
                                <input type="tel" maxlength="10" class="form-control" id="phoneNumber" placeholder="">
                                <span id="text-mobilemoney-message" class="form-text font-weight-bold mt-2"></span>
                            </div>
                            <div class="col-12 cursor" id="sendPhone">
                                <svg height="50" width="200" id="svgmobile">
                                <polygon points="15 25, 25 40, 150 40, 160 25, 150 10, 25 10" style="fill:#ffde08" id="polygonmobile"/>
                                <text x="65" y="30" fill="black">Payer</text>
                                Sorry, your browser does not support inline SVG.
                                </svg>
                            </div>
                        </div>
                        <div class="message-box" style="display: none;">
                            <div class="col-12">
                                Votre requête a été envoyée. Vous serez notifié de l'état de la transaction. <br>Merci
                            </div>
                        </div>
                    </div>
                    <div class="method border rounded p-3 mt-5 shadow-sm rounded"  id="methodcash">
                        <h1 class="font-weight-bold text-center">Module en cours de développement.</h1>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/jquery/dist/jquery.min.js"></script>
        <script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/JavaScript" src="assets/js/script.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- <script type="text/JavaScript" src="assets/js/pay.js" charset="UTF-8"></script> -->
        <script type="text/JavaScript" src="assets/js/Ajax.js" charset="UTF-8"></script>
        <script type="text/JavaScript" src="assets/js/pagetools.js" charset="UTF-8"></script>
        <script type="text/JavaScript" src="assets/js/validation.js" charset="UTF-8"></script>
        <script type="text/JavaScript" src="assets/js/Navigation.js" charset="UTF-8"></script>
        <script type="text/JavaScript" src="assets/js/dialog.js" charset="UTF-8"></script>

    </body>
</html>
